import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { RutaService } from '../../services/ruta.service';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { SharedService } from '../../services/sucursales.service';
import { Observable, Observer } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import { DatepickerOptions } from 'ng2-datepicker';
import * as esLocale from 'date-fns/locale/es';
import * as moment from 'moment';

@Component({
  templateUrl: 'ver-respuestas.component.html',
  styleUrls: ['./ver-respuestas.component.css','./foundation-theme.scss']
})
export class verRespuestasComponent {

  public cuestionarios: any = [];
  public datosBudget: any;
  public Budget: any = [];
  public Budget2: any = [];
  public Budget3: any = [];
  public selected: any = [];
  public datos: any;
  public datos1: any;
  public datos2: any;
  public datos3: any;
  public datos4: any;
  public isSelected = false;
  public cuestionarioDelete: any = {};
  public cuestionarioEdit: any = {
    cuest:{
      num_cuestionarios:0,
    },
  };
  public campanas:any = [];
  public campanas2:any = [];
  public sucursales:any = [];
  public sucursal_id: any;

  public answers: any = [];
  public question_unique: string = '';
  public question_open: string = '';
  public rpta_open: string = '';
  public cuestionario = {
    nombre:'',
    preguntas: []
  }
  public answers_edit: any = [];
  public question_uniqueE: string = '';
  public question_openE: string = '';
  public question_reference: string = '';
  public answers_reference: any = [];
  public create_cuestionario = {
    nombre: '',
    descripcion: '',
    cuestionario: '',
    estado_pago: 0,
    estado: 1
  }
  public respuestas_sin_verificar: any[] = [];
  public respuestas_aprobadas: any[] = [];
  public respuestas_rechazadas: any[] = [];
  public months = [
    {id:'01', nombre:'Enero'},
    {id:'02', nombre:'Febrero'},
    {id:'03', nombre:'Marzo'},
    {id:'04', nombre:'Abril'},
    {id:'05', nombre:'Mayo'},
    {id:'06', nombre:'Junio'},
    {id:'07', nombre:'Julio'},
    {id:'08', nombre:'Agosto'},
    {id:'09', nombre:'Septiembre'},
    {id:'10', nombre:'Octubre'},
    {id:'11', nombre:'Noviembre'},
    {id:'12', nombre:'Diciembre'}
  ]
  public anioActual: any;
  public mesActual: any;
  public anios = [];
  public fecha = new Date();
  public loading_activas: boolean = false;
  public loading_finalizadas: boolean = false;
  public loading_rechazadas: boolean = false;
  public empty_finalizadas: boolean = true;
  public empty_activas: boolean = true;
  public empty_rechazadas: boolean = true;

  public cuestionarioAprobar: any = {};
  public cuestionarioRechazar: any = {};

  acciones = false;

  constructor(private http: HttpClient, private router: Router, private ruta: RutaService, private builder: FormBuilder, private toastr: ToastrService, private spinnerService: Ng4LoadingSpinnerService, private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.sucursales = JSON.parse(localStorage.getItem('shoppers_sucursales'));
    this.sucursal_id = this.sucursales[0].id;
    this.getRespuestasSinVerificar();
  }

  getRespuestasSinVerificar() {
    this.http.get(this.ruta.get_ruta()+'evaluaciones/filter/estado/sucursal/'+this.sucursal_id+'?estado=1')
    .toPromise()
    .then(
    data => {
      this.datosBudget = data;
      this.Budget = this.datosBudget.respuestas;
      // this.http.get(this.ruta.get_ruta()+'sucursales/'+this.sucursal_id+'/campanas')
      // .toPromise()
      // .then(
      // data => {
      //   this.datos1 = data;
      //   this.campanas = this.datos1.sucursal.campanas;
      //   this.loading_activas = true;
      //   for (var i = 0; i < this.Budget.length; ++i) {
      //     //this.Budget[i].nombre = JSON.parse(this.Budget[i].cuestionario).nombre;
      //     this.Budget[i].nombre = this.Budget[i].cuest.nombre;
      //     if (this.Budget[i].estado == 1) {
      //       this.Budget[i].estado = 'Sin Verificar';
      //     } else if (this.Budget[i].estado == 2) {
      //       this.Budget[i].estado = 'Aprobado';
      //     } else if (this.Budget[i].estado == 3) {
      //       this.Budget[i].estado = 'Rechazado';
      //     }
      //     for (var j = 0; j < this.campanas.length; ++j) {
      //       if (this.campanas[j].id == this.Budget[i].campana_id) {
      //         this.Budget[i].campana = this.campanas[j].nombre;
      //       }
      //     }
      //     this.respuestas_sin_verificar.push(this.Budget[i]);
      //   }
      //   this.changeAprobadas();
      // },
      // msg => { 
      //   this.loading_activas = true;
      // });

      this.loading_activas = true;
        for (var i = 0; i < this.Budget.length; ++i) {
          //this.Budget[i].nombre = JSON.parse(this.Budget[i].cuestionario).nombre;
          this.Budget[i].nombre = this.Budget[i].cuest.nombre;
          if (this.Budget[i].estado == 1) {
            this.Budget[i].estado = 'Sin Verificar';
          } else if (this.Budget[i].estado == 2) {
            this.Budget[i].estado = 'Aprobado';
          } else if (this.Budget[i].estado == 3) {
            this.Budget[i].estado = 'Rechazado';
          }
          this.respuestas_sin_verificar.push(this.Budget[i]);
        }
        this.changeAprobadas();

    },
    msg => { 
      this.loading_activas = true;
      if (msg.status == 404) {
        this.empty_activas = false;
        this.changeAprobadas();
      }
    });
  }

  changeAprobadas(){
    this.http.get(this.ruta.get_ruta()+'evaluaciones/filter/estado/sucursal/'+this.sucursal_id+'?estado=2')
    .toPromise()
    .then(
    data => {
      this.datos2 = data;
      this.Budget2 = this.datos2.respuestas;
      this.loading_finalizadas = true;
      for (var i = 0; i < this.Budget2.length; ++i) {
        //this.Budget2[i].nombre = JSON.parse(this.Budget2[i].cuestionario).nombre;
        this.Budget2[i].nombre = this.Budget2[i].cuest.nombre;
        if (this.Budget2[i].estado == 1) {
          this.Budget2[i].estado = 'Sin Verificar';
        } else if (this.Budget2[i].estado == 2) {
          this.Budget2[i].estado = 'Aprobado';
        } else if (this.Budget2[i].estado == 3) {
          this.Budget2[i].estado = 'Rechazado';
        }
        // for (var j = 0; j < this.campanas.length; ++j) {
        //   if (this.campanas[j].id == this.Budget2[i].campana_id) {
        //     this.Budget2[i].campana = this.campanas[j].nombre;
        //   }
        // }
        this.respuestas_aprobadas.push(this.Budget2[i]);
      }
      this.respuestas_aprobadas.sort((a, b) => b.id - a.id);
      this.changeRechazadas();
    },
    msg => {
      this.loading_finalizadas = true; 
      if (msg.status == 404) {
        this.empty_finalizadas = false;
        this.changeRechazadas();
      }
    });
  }

  changeRechazadas(){
    this.http.get(this.ruta.get_ruta()+'evaluaciones/filter/estado/sucursal/'+this.sucursal_id+'?estado=3')
    .toPromise()
    .then(
    data => {
      this.datos3 = data;
      this.Budget3 = this.datos3.respuestas;
      this.loading_rechazadas = true;
      for (var i = 0; i < this.Budget3.length; ++i) {
        //this.Budget3[i].nombre = JSON.parse(this.Budget3[i].cuestionario).nombre;
        this.Budget3[i].nombre = this.Budget2[i].cuest.nombre;
        if (this.Budget3[i].estado == 1) {
          this.Budget3[i].estado = 'Sin Verificar';
        } else if (this.Budget3[i].estado == 2) {
          this.Budget3[i].estado = 'Aprobado';
        } else if (this.Budget3[i].estado == 3) {
          this.Budget3[i].estado = 'Rechazado';
        }
        // for (var j = 0; j < this.campanas.length; ++j) {
        //   if (this.campanas[j].id == this.Budget3[i].campana_id) {
        //     this.Budget3[i].campana = this.campanas[j].nombre;
        //   }
        // }
        this.respuestas_rechazadas.push(this.Budget3[i]);
      }
      this.respuestas_rechazadas.sort((a, b) => b.id - a.id);
    },
    msg => {
      this.loading_rechazadas = true; 
      if (msg.status == 404) {
        this.empty_rechazadas = false;
      }
    });
  }

  add_cuestionario(){
    this.router.navigate(['add_cuestionario'], {});
  }

  resetCampaign(){
    this.isSelected = false;
    this.cuestionarioDelete = {};
    this.cuestionario.preguntas = [];
    this.create_cuestionario.cuestionario = '';
    this.question_open = '';
    this.rpta_open = '';
    this.question_openE = '';
    this.question_uniqueE = '';
    this.question_reference = '';
    this.answers_edit = [];

    this.cuestionarioAprobar = {};
    this.cuestionarioRechazar = {};
  }

  resetCamp(){
    this.campanas = [];
    this.cuestionarios = [];
    this.respuestas_sin_verificar = [];
    this.respuestas_aprobadas = [];
    this.respuestas_rechazadas = [];
    this.Budget = [];
    this.Budget2 = [];
    this.loading_activas = false;
    this.loading_finalizadas = false;
    this.loading_rechazadas = false; 
    this.empty_finalizadas = true;
    this.empty_activas = true;
    this.empty_rechazadas = true;
  }

  showEdit(cuestionario){
    this.cuestionarioEdit = cuestionario;
    this.cuestionario.nombre = cuestionario.nombre;
    this.create_cuestionario.nombre = cuestionario.nombre;
    this.create_cuestionario.descripcion = cuestionario.cuest.descripcion;
    //this.create_cuestionario.estado_pago = cuestionario.estado_pago;
    if (cuestionario.estado == 'Sin Verificar') {
      this.create_cuestionario.estado = 1;
    } else if (cuestionario.estado == 'Aprobado') {
      this.create_cuestionario.estado = 2;
    } else if (cuestionario.estado == 'Rechazado') {
      this.create_cuestionario.estado = 3;
    }
    var question = JSON.parse(cuestionario.cuestionario);
    this.cuestionario.preguntas = question;
    let index = question.findIndex((item) => item.respuestas === '');
    if(index !== -1){
      this.question_open = question[index].pregunta;
      this.rpta_open = question[index].rpta;
    }
  }

  /* CUESTIONARIOS */
  addQuestion(){
    if (this.question_unique != '') {
      if (this.answers!=undefined && this.answers.length > 0) {
        let index = this.cuestionario.preguntas.findIndex((item) => item.pregunta === this.question_unique);
        if(index !== -1){
          this.toastr.error('Ya agregaste esta pregunta, por favor ingresa otra nueva','Error', {
                timeOut: 5000
              });
        } else {
          this.cuestionario.preguntas.push({pregunta: this.question_unique, respuestas: this.answers});
          this.answers = [];
          this.question_unique = '';
          this.question_uniqueE = '';
          this.question_reference = '';
          this.answers_edit = [];
        }
      } else {
        this.toastr.error('Debes asignar respuestas a la pregunta','Error', {
              timeOut: 5000
            });
      }
    } else {
      this.toastr.error('Debes completar el campo pregunta','Error', {
            timeOut: 5000
        });
    }
  }

  editQuestion(question){
    this.question_uniqueE = question.pregunta;
    this.question_reference = question.pregunta;
    this.answers_edit = question.respuestas;
  }

  updateQuestion(){
    if (this.question_uniqueE != '') {
      if (this.answers_edit!=undefined && this.answers_edit.length > 0) {
        for (var i = 0; i < this.cuestionario.preguntas.length; ++i) {
          if (this.cuestionario.preguntas[i].pregunta === this.question_reference) {
            this.cuestionario.preguntas[i].pregunta = this.question_uniqueE;
            this.cuestionario.preguntas[i].respuestas = this.answers_edit;
            document.getElementById('editU-campaign').click();
            this.question_uniqueE = '';
            this.question_reference = '';
            this.answers_edit = [];
          }
        }
      } else {
        this.toastr.error('Debes asignar respuestas a la pregunta','Error', {
              timeOut: 5000
            });
      }
    } else {
      this.toastr.error('Debes completar el campo pregunta','Error', {
            timeOut: 5000
        });
    }
  }

  deleteUQuestion(){
    let index = this.cuestionario.preguntas.findIndex((item) => item.pregunta === this.question_reference);
    if(index !== -1){
      this.cuestionario.preguntas.splice(index, 1);
      this.question_uniqueE = '';
      this.question_reference = '';
      this.answers_edit = [];
    }
  }

  editOQuestion(){
    this.question_openE = this.question_open;
  }

  updateOQuestion(){
    if (this.question_openE != '') {
      this.question_open = this.question_openE;
      document.getElementById('editO-campaign').click();
    } else {
      this.toastr.error('Debes completar el campo pregunta','Error', {
            timeOut: 5000
        });
    }
  }

  deleteOQuestion(){
    let index = this.cuestionario.preguntas.findIndex((item) => item.pregunta === this.question_open);
    if(index !== -1){
      this.cuestionario.preguntas.splice(index, 1);
      this.question_open = '';
      this.question_openE = '';
    }
  }

  EditCuestionario() {
    this.cuestionario.nombre = this.create_cuestionario.nombre;
    if (this.cuestionario.nombre!='') {
      if (this.cuestionario.preguntas.length > 0) {     
        let index = this.cuestionario.preguntas.findIndex((item) => item.pregunta === this.question_open);
        if(index == -1){
          if (this.question_open != '') {
            this.cuestionario.preguntas.push({pregunta: this.question_open, respuestas: ''});
          }
        }
        document.getElementById('modal-editar').click();
        this.spinnerService.show();
        this.create_cuestionario.cuestionario = JSON.stringify(this.cuestionario);
        this.http.put(this.ruta.get_ruta()+'cuestionarios/'+this.cuestionarioEdit.id, this.create_cuestionario)
            .toPromise()
            .then(
            data => {
              this.spinnerService.hide();
              this.toastr.success('Cuestionario editado con éxito','Éxito', {
                timeOut: 5000
              });
              this.resetCamp();
              this.resetCampaign();
              this.getRespuestasSinVerificar();
            },
            msg => {
              this.spinnerService.hide(); 
                this.toastr.error(msg.error.error,'Error', {
              timeOut: 5000
          });
        });
      } else {
        this.toastr.error('Debe asignar al menos una pregunta al cuestionario','Error', {
              timeOut: 5000
          });
          return false;
      }
    } else {
      this.toastr.error('Debe completar el campo nombre del cuestionario','Error', {
            timeOut: 5000
        });
        return false;
    }
  } 

  deleteCuestionario(cuestionarioDelete){
    this.spinnerService.show();
    this.http.delete(this.ruta.get_ruta()+'evaluaciones/'+cuestionarioDelete.id)
    .toPromise()
    .then(
    data => {
      this.spinnerService.hide();
      document.getElementById('modal-delete').click();
      this.resetCamp();
      this.resetCampaign();
      this.getRespuestasSinVerificar();
      this.datos = data;
      this.toastr.success(this.datos.message, 'Éxito', {
        timeOut: 5000
      });
    },
    msg => { 
      this.spinnerService.hide();
      this.toastr.error(msg.error.error, 'Error', {
        timeOut: 5000
      });
    });
  } 

  update_sucursal(event){
    this.sucursal_id = event.target.value;
    this.campanas = [];
    this.Budget = [];
    this.campanas2 = [];
    this.Budget2 = [];
    this.respuestas_sin_verificar = [];
    this.respuestas_aprobadas = [];
    this.respuestas_rechazadas = [];
    this.loading_activas = false;
    this.loading_finalizadas = false;
    this.empty_finalizadas = true;
    this.empty_activas = true;
    this.getRespuestasSinVerificar();
  }  

  aprobarCuestionario(cuestionarioAprobar){

    var datos= {
      // token: localStorage.getItem('mouvers_token'),
      estado: 2,
    }

    //evaluaciones/pagar/{cliente_id}/{empresa_id}/{cuestionario_id}/{evaluacion_id}

    this.spinnerService.show();
    this.http.post(this.ruta.get_ruta()+'evaluaciones/pagar/'+cuestionarioAprobar.cliente.id+'/'+cuestionarioAprobar.sucursal.empresa_id+'/'+cuestionarioAprobar.cuest.id+'/'+cuestionarioAprobar.id,datos)
    .toPromise()
    .then(
    data => {
      this.spinnerService.hide();
      //document.getElementById('modal-delete').click();
      this.resetCamp();
      this.resetCampaign();
      this.getRespuestasSinVerificar();
      this.datos = data;
      this.toastr.success(this.datos.message, 'Éxito', {
        timeOut: 5000
      });
    },
    msg => { 
      this.spinnerService.hide();
      this.toastr.error(msg.error.error, 'Error', {
        timeOut: 5000
      });
    });
  } 

  rechazarCuestionario(cuestionarioRechazar){

    var datos= {
      // token: localStorage.getItem('mouvers_token'),
      estado: 3,
    }

    this.spinnerService.show();
    this.http.put(this.ruta.get_ruta()+'evaluaciones/'+cuestionarioRechazar.id,datos)
    .toPromise()
    .then(
    data => {
      this.spinnerService.hide();
      //document.getElementById('modal-delete').click();
      this.resetCamp();
      this.resetCampaign();
      this.getRespuestasSinVerificar();
      this.datos = data;
      this.toastr.success(this.datos.message, 'Éxito', {
        timeOut: 5000
      });
    },
    msg => { 
      this.spinnerService.hide();
      this.toastr.error(msg.error.error, 'Error', {
        timeOut: 5000
      });
    });
  }

}
