import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { verRespuestasComponent } from './ver-respuestas.component';

const routes: Routes = [
  {
    path: '',
    component: verRespuestasComponent,
    data: {
      title: 'Ver respuestas'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class verRespuestasRoutingModule {}
