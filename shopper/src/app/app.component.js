var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, IonicApp, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { StorageProvider } from '../providers/storage/storage';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../providers/rute-base/rute-base';
import { Keyboard } from '@ionic-native/keyboard';
var MyApp = /** @class */ (function () {
    function MyApp(platform, ionicApp, statusBar, splashScreen, storage, http, rutebaseAPI, app, keyboard) {
        var _this = this;
        this.ionicApp = ionicApp;
        this.storage = storage;
        this.http = http;
        this.rutebaseAPI = rutebaseAPI;
        this.app = app;
        this.keyboard = keyboard;
        this.rootPage = LoginPage;
        var item = this.storage.get('tokenShopper');
        if (item !== null && item !== "") {
            this.rootPage = TabsPage;
        }
        else {
            this.rootPage = LoginPage;
        }
        platform.ready().then(function () {
            statusBar.styleLightContent();
            setTimeout(function () {
                splashScreen.hide();
            }, 1000);
            keyboard.onKeyboardShow().subscribe(function () {
                document.body.classList.add('keyboard-is-open');
            });
            keyboard.onKeyboardHide().subscribe(function () {
                document.body.classList.remove('keyboard-is-open');
            });
            var OneSignal = window["plugins"].OneSignal;
            //var that = this;
            OneSignal
                .startInit("32500e5f-2def-4a7a-96bf-541ceb67df76", "275279573191")
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .handleNotificationOpened(function (jsonData) {
                //if(jsonData.notification.payload.additionalData.accion == 1){
                //var sucursal = JSON.parse(jsonData.notification.payload.additionalData.obj);
                //var cuestionario_id = jsonData.notification.payload.additionalData.cuestionario_id;
                //that.nav.push(AcceptQuestionPage, {cuestionario_id: cuestionario_id, nombre: sucursal.sucursal, logo: sucursal.logo});      
                //}
            })
                .handleNotificationReceived(function (jsonData) {
                //if(jsonData.payload.additionalData.accion == 1){
                //var sucursal = JSON.parse(jsonData.payload.additionalData.obj);
                //var cuestionario_id = jsonData.payload.additionalData.cuestionario_id;
                //that.nav.push(AcceptQuestionPage, {cuestionario_id: cuestionario_id, nombre: sucursal.sucursal, logo: sucursal.logo});       
                //}
            })
                .endInit();
        });
        platform.registerBackButtonAction(function () {
            var nav = app.getActiveNavs()[0];
            var activePortal = _this.ionicApp._loadingPortal.getActive() ||
                _this.ionicApp._modalPortal.getActive() ||
                _this.ionicApp._toastPortal.getActive() ||
                _this.ionicApp._overlayPortal.getActive();
            if (activePortal) {
                activePortal.instance.backButtonAction();
                return;
            }
            else if (nav.canGoBack()) {
                nav.pop();
                return;
            }
            else {
                platform.exitApp();
                return;
            }
        });
    }
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform, IonicApp, StatusBar, SplashScreen, StorageProvider, HttpClient, RuteBaseProvider, App, Keyboard])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map