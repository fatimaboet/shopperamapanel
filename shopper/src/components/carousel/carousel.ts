import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';

export interface CourselItem {
  empresa_imagen?: string;
  cuestionario_id: number;
  campana_id: number;
  nombre_empresa: string;
  nombre_campana: string;
}

interface SlideItem {
  idx: number;
  cuestionario_id: number;
  empresa_imagen: string;
  currentPlacement: number;
  campana_id: number;
  nombre_empresa: string;
  nombre_campana: string;
}


@Component({
  selector: 'carousel',
  templateUrl: 'carousel.html'
})
export class CarouselComponent {
  private currentDeg: number = 0;
  private items: Array<SlideItem> = [];
  private tz: number;

  @Input() set slides(values: Array<CourselItem>) {
    if (!values.length) return;

    let degree: number = 0;
    this.tz = 200;//Math.round((this.containerWidth / 2) /
      //Math.tan(Math.PI / values.length));
    setTimeout(function () {
      this.items = <Array<SlideItem>>values.map((item: CourselItem, index: number) => {
      let slideItem = {
        idx: index,
        cuestionario_id: item.cuestionario_id,
        campana_id: item.campana_id,
        nombre_empresa: item.nombre_empresa,
        empresa_imagen: item.empresa_imagen,
        currentPlacement: degree,
        nombre_campana: item.nombre_campana
      };
      degree = degree + 60;
      console.log(slideItem);
      return slideItem;
    })
    }, 5000)

    
    
  }

  @Output() selectSlide = new EventEmitter();

  constructor(private eleRef: ElementRef) {
   }

  onSwipeLeft(event,item) {
    if (item.idx != this.items.length-1) {
      this.currentDeg = this.currentDeg - 60;
      this.applyStyle();
    } 
  }

  onSwipeRight(event,item) {
    if (item.idx != 0) {
      this.currentDeg = this.currentDeg + 60;
      this.applyStyle();
    }      
  }

  private applyStyle() {
    let ele = this.eleRef.nativeElement.querySelector('.carousel');
    ele.style[ '-webkit-transform' ] = "rotateY(" + this.currentDeg + "deg)";
    ele.style[ '-moz-transform' ] = "rotateY(" + this.currentDeg + "deg)";
    ele.style[ '-o-transform' ] = "rotateY(" + this.currentDeg + "deg)";
    ele.style[ 'transform' ] = "rotateY(" + this.currentDeg + "deg)";
  }

  selectItem(item:any){
    this.selectSlide.emit(item);
  }
} 
