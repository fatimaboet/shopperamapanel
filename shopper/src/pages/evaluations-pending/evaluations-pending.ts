import { Component } from '@angular/core';
import { App, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { EvaluationDetailPage } from '../evaluation-detail/evaluation-detail';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'page-evaluations-pending',
  templateUrl: 'evaluations-pending.html',
})
export class EvaluationsPendingPage {

	public datos:any;
	public evaluations:any = [];
  public item:any = [];
	public user: any;
  public client_id:any;
  public searchTerm: string = '';
  public searchControl: FormControl;
  public searching: any = false;
  public preferences: any = [];
  groupedData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient, public rutebaseAPI: RuteBaseProvider,public elementService: ElementsServicesProvider,public storage: StorageProvider, public app: App) {
  	this.user = this.storage.getObject('userShopper');
    this.client_id = this.user.cliente.id;
    this.searchControl = new FormControl();
  }

  ionViewWillEnter(){
    this.initEvaluations();
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.setFilteredItems();
    });
  }

  onSearchInput(){
   this.searching = true;
  }

  setFilteredItems() {
   this.evaluations = this.filterItems(this.searchTerm);
  }

  filterItems(searchTerm){
    if (searchTerm != '') {
      let vihicledata= this.item;
      let items = [];
      var array = Object.keys(vihicledata).map(function(k) {
        return vihicledata[k]
      });

      for(let i =0 ;i < array.length;i++){
        array[i].filter((item) => {
          if(item.cuest.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1){
            items.push(item);
          } else {
            if(item.sucursal.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1){
              items.push(item);
            }
          }
          
        });     
      }
      return this.convertTo(items,'created_at', true); 
    } else {
      return this.item;
    }
    
  }

  initEvaluations(){
  	//this.elementService.showLoading('Cargando Evaluaciones...');
    this.http.get(this.rutebaseAPI.getRutaApi()+'evaluaciones/respondidas/'+this.client_id+'?token='+this.storage.get('tokenShopper'))
    .toPromise()
    .then(
      data => {
        this.datos = data;
        //this.item = this.datos.respuestas;
        //console.log(this.item);
        this.item = this.convertTo(this.datos.respuestas,'created_at', true);
        this.setFilteredItems();
        //this.elementService.hideLoading();
        this.getPreference();       
        
      },
      msg => {
        //this.elementService.hideLoading();
        if(msg.status == 400 || msg.status == 401){ 
          this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
          this.app.getRootNav().setRoot(LoginPage);
        } else {
          this.elementService.presentToast(msg.error.error);
        }
    });
  }
  
  convertTo(arr, key, dayWise) {
    var groups = {};
    for (var i=0; i < arr.length; i++) {
      if (dayWise) {
        if (arr[i][key].indexOf(':') > -1)
        {
          arr[i][key] = new Date(arr[i][key].replace(' ', 'T')).toLocaleDateString();
        }       
      }
      else {
        arr[i][key] = new Date(arr[i][key]).toTimeString();
      }
      groups[arr[i][key]] = groups[arr[i][key]] || [];
      groups[arr[i][key]].push(arr[i]);
    }
    return groups;
  };

  getPreference(){
    this.http.get(this.rutebaseAPI.getRutaApi()+'clientes/'+this.client_id+'?token='+this.storage.get('tokenShopper'))
    .toPromise()
    .then(
      data => {
        this.datos = data;
        this.preferences = this.datos.cliente.preferencias;
      },
      msg => {
        this.elementService.hideLoading();
        if(msg.status == 400 || msg.status == 401){ 
          this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
          this.app.getRootNav().setRoot(LoginPage);
        } else {
          this.elementService.presentToast(msg.error.error);
          console.log(this.evaluations.length);
        }
    });
  }

  evaluationDetail(evaluation){
    console.log(evaluation);
    this.navCtrl.push(EvaluationDetailPage, {respuesta_id: evaluation.id});
  }

}
