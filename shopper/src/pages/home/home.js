var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WalletPage } from '../wallet/wallet';
import { PollPage } from '../poll/poll';
import { OneSignal } from '@ionic-native/onesignal';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, storage, rutebaseApi, http, eleRef, alertCtrl, oneSignal) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.rutebaseApi = rutebaseApi;
        this.http = http;
        this.eleRef = eleRef;
        this.alertCtrl = alertCtrl;
        this.oneSignal = oneSignal;
        this.questions = [];
        this.campanas = [];
        this.slides = [];
        this.datos = [];
        this.usuario = {
            nombre: '',
            imagen: 'assets/imgs/user.png'
        };
        this.currentDeg = 0;
        this.balance = 0;
        this.evaluations_empty = false;
        this.update_client = {
            customer_id: ''
        };
        this.cliente = {
            "name": "",
            "email": "",
            "external_id": ""
        };
        this.token = {
            token_notificacion: null
        };
        OpenPay.setId('mlfpboaflbbaboev97wm');
        OpenPay.setApiKey('pk_9d474dec96e54168b9eba537b8b65f02');
        OpenPay.setSandboxMode(true);
        this.user = this.storage.getObject('userShopper');
        console.log(this.storage.getObject('userShopper'));
        this.usuario.nombre = this.user.nombre;
        this.usuario.imagen = this.user.cliente.imagen;
        this.client_id = this.user.cliente.id;
        this.tz = 210;
    }
    HomePage.prototype.ionViewWillEnter = function () {
        this.user = this.storage.getObject('userShopper');
        this.usuario.nombre = this.user.nombre;
        this.usuario.imagen = this.user.cliente.imagen;
        this.client_id = this.user.cliente.id;
        this.getQuestions();
    };
    HomePage.prototype.ionViewDidEnter = function () {
        this.getInfo();
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.notification();
    };
    HomePage.prototype.getQuestions = function () {
        var _this = this;
        this.questions = [];
        this.http.get(this.rutebaseApi.getRutaApi() + 'clientes/campanas/pendientes/' + this.client_id)
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            _this.campanas = _this.datos.campanas;
            var degree = 0;
            var index = 0;
            for (var i = 0; i < _this.campanas.length; ++i) {
                for (var j = 0; j < _this.campanas[i].cuest.length; ++j) {
                    _this.questions.push({
                        idx: index,
                        cuestionario_id: _this.campanas[i].cuest[j].id,
                        campana_id: _this.campanas[i].cuest[j].campana_id,
                        nombre_empresa: _this.campanas[i].empresa.usuario.nombre,
                        empresa_imagen: _this.campanas[i].empresa.imagen,
                        nombre_cuestionario: _this.campanas[i].cuest[j].nombre,
                        descripcion_cuestionario: _this.campanas[i].cuest[j].descripcion,
                        currentPlacement: degree,
                    });
                    index = index + 1;
                    degree = degree + 60;
                }
            }
            if (_this.campanas.length > 0) {
                _this.evaluations_empty = false;
            }
            else {
                _this.evaluations_empty = true;
            }
        }, function (msg) {
        });
    };
    HomePage.prototype.getInfo = function () {
        var _this = this;
        this.http.get(this.rutebaseApi.getRutaApi() + 'sistema/tarifas/pk')
            .toPromise()
            .then(function (data) {
            _this.datos5 = data;
            if (_this.user.cliente.customer_id != null) {
                var headers = new HttpHeaders();
                headers = headers.append("Authorization", "Basic " + btoa(_this.datos5.tarifas.tarifa_sk + ":"));
                headers = headers.append("Content-Type", "application/json");
                _this.http.get(_this.rutebaseApi.getRuteOpen() + 'customers/' + _this.user.cliente.customer_id, {
                    headers: headers
                })
                    .toPromise()
                    .then(function (data) {
                    console.log(data);
                    _this.datos1 = data;
                    _this.balance = _this.datos1.balance.toFixed(2);
                }, function (msg) {
                });
            }
            else {
                _this.cliente.name = _this.user.nombre;
                _this.cliente.email = _this.user.email;
                _this.cliente.external_id = _this.user.id;
                var headers = new HttpHeaders();
                headers = headers.append("Authorization", "Basic " + btoa(_this.datos5.tarifas.tarifa_sk + ":"));
                headers = headers.append("Content-Type", "application/json");
                _this.http.post(_this.rutebaseApi.getRuteOpen() + 'customers', _this.cliente, {
                    headers: headers
                })
                    .toPromise()
                    .then(function (data) {
                    _this.datos1 = data;
                    _this.update_client.customer_id = _this.datos1.id;
                    _this.http.put(_this.rutebaseApi.getRutaApi() + 'clientes/' + _this.client_id, _this.update_client)
                        .toPromise()
                        .then(function (data) {
                        console.log(data);
                        _this.datosU = data;
                        var storage = _this.storage.getObject('userShopper');
                        storage.cliente = _this.datosU.cliente;
                        _this.storage.setObject('userShopper', storage);
                        _this.presentAlert();
                    }, function (msg) {
                    });
                }, function (msg) {
                });
            }
            ;
        }, function (msg) {
        });
    };
    ;
    HomePage.prototype.notification = function () {
        var _this = this;
        this.oneSignal.getIds().then(function (ids) {
            _this.token.token_notificacion = ids.userId;
            _this.http.put(_this.rutebaseApi.getRutaApi() + 'clientes/tokennotificacion/' + _this.user.id, _this.token)
                .toPromise()
                .then(function (data) {
                console.log(data);
            }, function (msg) {
            });
        });
    };
    HomePage.prototype.presentAlert = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Afiliar cuenta bancaria',
            message: '¡Para disfrutar del saldo que acumules por tus evaluaciones debes asociar una cuenta bancaria y retirar los fondos!',
            buttons: [
                {
                    text: 'Ver más tarde',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Ir',
                    handler: function () {
                        _this.navCtrl.push(WalletPage);
                    }
                }
            ]
        });
        alert.present();
    };
    HomePage.prototype.onSwipeLeft = function (event, item) {
        if (item.idx != this.questions.length - 1) {
            this.currentDeg = this.currentDeg - 60;
            this.applyStyle();
        }
    };
    HomePage.prototype.onSwipeRight = function (event, item) {
        if (item.idx != 0) {
            this.currentDeg = this.currentDeg + 60;
            this.applyStyle();
        }
    };
    HomePage.prototype.swipeEvent = function (e, item) {
        if (e.direction == 2) {
            if (item.idx != this.questions.length - 1) {
                this.currentDeg = this.currentDeg - 60;
                this.applyStyle();
            }
        }
        if (e.direction == 4) {
            if (item.idx != 0) {
                this.currentDeg = this.currentDeg + 60;
                this.applyStyle();
            }
        }
    };
    HomePage.prototype.applyStyle = function () {
        var ele = this.eleRef.nativeElement.querySelector('.carousel');
        ele.style['-webkit-transform'] = "rotateY(" + this.currentDeg + "deg)";
        ele.style['-moz-transform'] = "rotateY(" + this.currentDeg + "deg)";
        ele.style['-o-transform'] = "rotateY(" + this.currentDeg + "deg)";
        ele.style['transform'] = "rotateY(" + this.currentDeg + "deg)";
    };
    HomePage.prototype.acceptQuestion = function (item) {
        this.navCtrl.push(PollPage, { cuestionario_id: item.cuestionario_id, logo: item.empresa_imagen, nombre: item.nombre_empresa, campana_id: item.campana_id, client_id: this.client_id });
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html',
        }),
        __metadata("design:paramtypes", [NavController, StorageProvider, RuteBaseProvider, HttpClient, ElementRef, AlertController, OneSignal])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map