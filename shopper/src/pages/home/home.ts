import { Component, ElementRef } from '@angular/core';
import { NavController, AlertController, App, ToastController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { HttpClient } from '@angular/common/http';
import { WalletPage } from '../wallet/wallet';
import { PollPage } from '../poll/poll';
import { OneSignal } from '@ionic-native/onesignal';
import { LoginPage } from '../login/login';

declare var OpenPay;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public user: any;
  public questions: any = [];
  public campanas: any = [];
  public slides: any = [];
  public datos: any = [];
  public datos1: any;
  public datosU: any;
  public datos5: any;
  public client_id:any;
  public show_a: boolean = false;
  public usuario = {
    nombre: '',
    imagen: 'assets/imgs/user.png'
  };
  private currentDeg: number = 0;
  private tz: number;
  public balance: number = 0;
  public evaluations_empty: boolean = false;
  private update_client = {
    customer_id: ''
  }
  private cliente = {
    "name":"",
    "email":"",
    "external_id":"",
    "token": "",
    "requires_account": true
  }
  public token = {
    token_notificacion: null
  }

  constructor(public navCtrl: NavController, 
    public storage: StorageProvider,
    private rutebaseApi: RuteBaseProvider, 
    public http: HttpClient, 
    private eleRef: ElementRef, 
    private alertCtrl: AlertController, 
    private oneSignal: OneSignal,
    public app: App,
    private toastCtrl: ToastController) {
    this.user = this.storage.getObject('userShopper');
    this.usuario.nombre = this.user.nombre;
    this.usuario.imagen = this.user.cliente.imagen;
    this.client_id = this.user.cliente.id;
    this.tz = 210;
  }

  ionViewWillEnter(){
    this.currentDeg = 0;
    this.applyStyle();
    this.user = this.storage.getObject('userShopper');
    this.usuario.nombre = this.user.nombre;
    this.usuario.imagen = this.user.cliente.imagen;
    this.client_id = this.user.cliente.id;
    this.getQuestions();    
  }

  ionViewDidEnter(){
    this.getInfo();
  }

  ionViewDidLoad(){
    this.notification();
  }

  getQuestions(){
    this.questions = [];
    this.http.get(this.rutebaseApi.getRutaApi() + 'clientes/campanas/pendientes/'+this.client_id+'?token='+this.storage.get('tokenShopper'))
    .toPromise()
    .then(
    data => {
      this.datos = data;
      this.campanas = this.datos.campanas;
      let degree: number = 0;
      let index: number = 0;
      for (var i = 0; i < this.campanas.length; ++i) {
        for (var j = 0; j < this.campanas[i].cuest.length; ++j) {
          this.questions.push({
            idx: index,
            cuestionario_id: this.campanas[i].cuest[j].id,
            campana_id: this.campanas[i].cuest[j].campana_id,
            nombre_empresa: this.campanas[i].empresa.usuario.nombre,
            empresa_imagen: this.campanas[i].empresa.imagen,
            nombre_cuestionario: this.campanas[i].cuest[j].nombre,
            descripcion_cuestionario: this.campanas[i].cuest[j].descripcion,
            currentPlacement: degree,
          })
          index = index + 1;
          degree = degree + 60;
        }
      }
      if (this.campanas.length > 0) {
        this.evaluations_empty = false;
      } else {
        this.evaluations_empty = true;
      }     
    },
    msg => {
    });
  }

  getInfo(){
    this.show_a = false;
    console.log(this.user)
    if (this.user.cliente.customer_id != null) {  
      this.http.get(this.rutebaseApi.getRutaApi()+'pagos/openpay/customer/'+this.user.cliente.customer_id+'?token='+this.storage.get('tokenShopper'))
      .toPromise()
      .then(
      data => {
        console.log(data)
        this.datos1 = data;
        if (this.datos1.openpay.balance) {
          this.balance = this.datos1.openpay.balance.toFixed(2);
        }
        this.show_a = true;
      },
      msg => {
        if(msg.status == 400 || msg.status == 401){ 
          this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
          this.app.getRootNavs()[0].setRoot(LoginPage);
        } else {
          this.presentToast(msg.error.error);
        }
      });
    } else {
      this.show_a = true;
      this.cliente.name = this.user.nombre;
      this.cliente.email = this.user.email;
      this.cliente.external_id = this.user.id;
      this.cliente.token = this.storage.get('tokenShopper');
      this.http.post(this.rutebaseApi.getRutaApi()+'pagos/openpay/customers', this.cliente)
      .toPromise()
      .then(
      data => {
        console.log(data)
        this.datos1 = data;
        if (this.datos1.openpay.id) {
          this.update_client.customer_id = this.datos1.openpay.id;
          this.http.put(this.rutebaseApi.getRutaApi()+'clientes/'+this.client_id+'?token='+this.storage.get('tokenShopper'), this.update_client)
          .toPromise()
          .then(
          data => {
            this.datosU = data;
            let storage = this.storage.getObject('userShopper');
            storage.cliente = this.datosU.cliente;
            this.storage.setObject('userShopper', storage);
            this.presentAlert();
          },
          msg => {
            if(msg.status == 400 || msg.status == 401){ 
              this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
              this.app.getRootNavs()[0].setRoot(LoginPage);
            } else {
              //this.presentToast(msg.error.error);
            } 
          });
        } else if (this.datos1.openpay.http_code == 409){
          this.cliente.name = this.user.nombre;
          this.cliente.email = this.user.email;
          this.cliente.external_id = 'a'+this.user.id;
          this.cliente.token = this.storage.get('tokenShopper');
          this.http.post(this.rutebaseApi.getRutaApi()+'pagos/openpay/customers', this.cliente)
          .toPromise()
          .then(
          data => {
            console.log(data)
            this.datos1 = data;
            if (this.datos1.openpay.id) {
              this.update_client.customer_id = this.datos1.openpay.id;
              this.http.put(this.rutebaseApi.getRutaApi()+'clientes/'+this.client_id+'?token='+this.storage.get('tokenShopper'), this.update_client)
              .toPromise()
              .then(
              data => {
                this.datosU = data;
                let storage = this.storage.getObject('userShopper');
                storage.cliente = this.datosU.cliente;
                this.storage.setObject('userShopper', storage);
                this.presentAlert();
              },
              msg => {
                if(msg.status == 400 || msg.status == 401){ 
                  this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
                  this.app.getRootNavs()[0].setRoot(LoginPage);
                } else {
                  //this.presentToast(msg.error.error);
                } 
              });
            }
          },
          msg => {
            if(msg.status == 400 || msg.status == 401){ 
              this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
              this.app.getRootNavs()[0].setRoot(LoginPage);
            } else {
              this.presentToast(msg.error.error);
            }
          });
        }
      },
      msg => {
        if(msg.status == 400 || msg.status == 401){ 
          this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
          this.app.getRootNavs()[0].setRoot(LoginPage);
        } else {
          this.presentToast(msg.error.error);
        }
      });
    };
  };

  notification(){
    this.oneSignal.getIds().then((ids) => {
      this.token.token_notificacion = ids.userId;
      this.http.put(this.rutebaseApi.getRutaApi()+'clientes/tokennotificacion/'+this.user.id+'?token='+this.storage.get('tokenShopper'), this.token)
      .toPromise()
      .then(
      data => {
        console.log(data);
      },
      msg => { 
      });
    });
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Afiliar cuenta bancaria',
      message: '¡Para disfrutar del saldo que acumules por tus evaluaciones debes asociar una cuenta bancaria y retirar los fondos!',
      buttons: [
        {
          text: 'Ver más tarde',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ir',
          handler: () => {
            this.navCtrl.push(WalletPage);
          }
        }
      ]
    });
    alert.present();
  }

  onSwipeLeft(event,item) {
    if (item.idx != this.questions.length-1) {
      this.currentDeg = this.currentDeg - 60;
      this.applyStyle();
    } 
  }

  onSwipeRight(event,item) {
    if (item.idx != 0) {
      this.currentDeg = this.currentDeg + 60;
      this.applyStyle();
    }      
  }

  swipeEvent(e,item) {
    if (e.direction == 2) {
      if (item.idx != this.questions.length-1) {
        this.currentDeg = this.currentDeg - 60;
        this.applyStyle();
      } 
    }
    if (e.direction == 4) {
      if (item.idx != 0) {
        this.currentDeg = this.currentDeg + 60;
        this.applyStyle();
      }  
    }
  }

  private applyStyle() {
    let ele = this.eleRef.nativeElement.querySelector('.carousel');
    ele.style[ '-webkit-transform' ] = "rotateY(" + this.currentDeg + "deg)";
    ele.style[ '-moz-transform' ] = "rotateY(" + this.currentDeg + "deg)";
    ele.style[ '-o-transform' ] = "rotateY(" + this.currentDeg + "deg)";
    ele.style[ 'transform' ] = "rotateY(" + this.currentDeg + "deg)";
  }

  acceptQuestion(item){
    this.navCtrl.push(PollPage, {cuestionario_id: item.cuestionario_id, logo: item.empresa_imagen, nombre: item.nombre_empresa, campana_id: item.campana_id, client_id: this.client_id});
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
