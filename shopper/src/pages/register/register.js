var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ModalPreferencePage } from '../modal-preference/modal-preference';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { StorageProvider } from '../../providers/storage/storage';
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, http, navParams, alertCtrl, loadingCtrl, builder, toastCtrl, modalCtrl, auth, rutebaseApi, storage) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.builder = builder;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.auth = auth;
        this.rutebaseApi = rutebaseApi;
        this.storage = storage;
        this.password_type1 = 'password';
        this.password_type2 = 'password';
        this.city = 'Municipio';
        this.municipios = [];
        this.ciudades = [];
        this.colonias = [];
        this.dataPreference = [];
        this.createSuccess = false;
        this.preferences = [];
        this.formErrors = {
            'nombre': '',
            'email': '',
            'ciudad': '',
            'estado': '',
            'password': '',
            'rpassword': '',
            'edad': ''
        };
        this.sexos = [{ id: 0, tipo: 'Femenino' }, { id: 1, tipo: 'Masculino' }];
        this.initForm();
    }
    RegisterPage.prototype.initForm = function () {
        var _this = this;
        this.registerUserForm = this.builder.group({
            nombre: ['', [Validators.required, Validators.minLength(2)]],
            email: ['', [Validators.required, Validators.email]],
            edad: ['', [Validators.required]],
            tipo_registro: [1],
            sexo: ['Femenino'],
            estado_id: [''],
            municipio_id: [''],
            localidad_id: [''],
            imagen: ['http://shopper.internow.com.mx/images/user.png'],
            password: ['', [Validators.required]],
            rpassword: ['', [Validators.required]],
            preferencias: ['']
        });
        this.registerUserForm.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
        this.http.get(this.rutebaseApi.getRutaApi() + 'categorias')
            .toPromise()
            .then(function (data) {
            _this.dataPreference = data;
            _this.page();
        }, function (msg) {
            _this.page();
            _this.presentToast('No se pudo cargar las preferencias, ingresa de nuevo', 1500);
        });
    };
    RegisterPage.prototype.page = function () {
        var _this = this;
        this.showLoadingc();
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/estados')
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            _this.estados = _this.datos.estados;
            console.log(data);
            _this.registerUserForm.patchValue({ estado_id: _this.estados[0].id });
            _this.setEstado(_this.estados[0].id);
            _this.loading.dismiss();
        }, function (msg) {
            _this.presentToast('No se pudo cargar los estados y ciudades, ingresa de nuevo', 1500);
            _this.loading.dismiss();
        });
    };
    RegisterPage.prototype.setEstado = function (estado) {
        this.municipios = [];
        this.colonias = [];
        this.initEstados(estado);
        if (estado == 9) {
            this.city = 'Delegación';
        }
        else {
            this.city = 'Municipio';
        }
    };
    RegisterPage.prototype.initEstados = function (estado_id) {
        var _this = this;
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/municipios?estado_id=' + estado_id)
            .toPromise()
            .then(function (data) {
            _this.datos1 = data;
            _this.municipios = _this.datos1.municipios;
            _this.registerUserForm.patchValue({ municipio_id: _this.municipios[0].id });
            _this.http.get(_this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id=' + _this.municipios[0].id)
                .toPromise()
                .then(function (data) {
                _this.datos2 = data;
                _this.colonias = _this.datos2.localidades;
                _this.registerUserForm.patchValue({ localidad_id: _this.colonias[0].id });
            }, function (msg) {
                _this.presentToast('No se pudo cargar las colonias, intenta de nuevo', 1500);
            });
        }, function (msg) {
            _this.presentToast('No se pudo cargar los municipios, intenta de nuevo', 1500);
        });
    };
    RegisterPage.prototype.setMunicipio = function (event) {
        var _this = this;
        this.colonias = [];
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id=' + event)
            .toPromise()
            .then(function (data) {
            _this.datos2 = data;
            _this.colonias = _this.datos2.localidades;
            _this.registerUserForm.patchValue({ localidad_id: _this.colonias[0].id });
        }, function (msg) {
            _this.presentToast('No se pudo cargar las colonias, intenta de nuevo', 1500);
        });
    };
    RegisterPage.prototype.setColonia = function (event) {
        this.registerUserForm.patchValue({ localidad_id: event });
    };
    RegisterPage.prototype.togglePasswordMode1 = function () {
        this.password_type1 = this.password_type1 === 'text' ? 'password' : 'text';
    };
    RegisterPage.prototype.togglePasswordMode2 = function () {
        this.password_type2 = this.password_type2 === 'text' ? 'password' : 'text';
    };
    RegisterPage.prototype.presentModal = function () {
        var _this = this;
        if (this.dataPreference != '') {
            var profileModal = this.modalCtrl.create(ModalPreferencePage, { prefer: this.preferences, collec: this.dataPreference.categorias });
            profileModal.onDidDismiss(function (data) {
                _this.preferences = data;
            });
            profileModal.present();
        }
        else {
            this.presentToast('No se pudieron cargar las preferencias ingresa de nuevo', 1500);
        }
    };
    RegisterPage.prototype.deleteChip = function (prefer) {
        var index = this.preferences.findIndex(function (item1) { return item1.nombre === prefer.nombre; });
        if (index !== -1) {
            this.preferences.splice(index, 1);
        }
        for (var i = 0; i < this.dataPreference.categorias.length; ++i) {
            if (this.dataPreference.categorias[i].nombre == prefer.nombre) {
                this.dataPreference.categorias[i].checked = false;
            }
        }
    };
    RegisterPage.prototype.register = function () {
        var _this = this;
        this.registerUserForm.value.preferencias = JSON.stringify(this.preferences);
        this.registerUserForm.value.email = this.registerUserForm.value.email.toLowerCase();
        if (this.registerUserForm.valid) {
            if (this.registerUserForm.value.password !== this.registerUserForm.value.rpassword) {
                this.presentToast("¡Lo sentimos!, Contraseñas no coinciden", 1500);
            }
            else {
                this.showLoading();
                this.auth.register(this.registerUserForm.value).subscribe(function (success) {
                    if (success) {
                        _this.loading.dismiss();
                        _this.createSuccess = true;
                        _this.showPopup("Completado", 'Usuario registrado con éxito');
                    }
                    else {
                        _this.presentToast("Ha ocurrido un error al crear la cuenta.", 1500);
                    }
                }, function (error) {
                    _this.loading.dismiss();
                    _this.presentToast(error.error, 1500);
                });
            }
        }
        else {
            this.validateAllFormFields(this.registerUserForm);
            this.presentToast('¡Faltan datos para el registro!', 1500);
        }
    };
    RegisterPage.prototype.onValueChanged = function (data) {
        if (!this.registerUserForm) {
            return;
        }
        var form = this.registerUserForm;
        for (var field in this.formErrors) {
            var control = form.get(field);
            this.formErrors[field] = '';
            if (control && control.dirty && !control.valid) {
                for (var key in control.errors) {
                    this.formErrors[field] += true;
                    console.log(key);
                }
            }
        }
    };
    RegisterPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsDirty({ onlySelf: true });
                _this.onValueChanged();
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    };
    RegisterPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Registrando Usuario...',
            spinner: 'ios',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    RegisterPage.prototype.showLoadingc = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Cargando...',
            spinner: 'ios'
        });
        this.loading.present();
    };
    RegisterPage.prototype.showPopup = function (title, text) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons: [
                {
                    text: 'OK',
                    handler: function (data) {
                        if (_this.createSuccess) {
                            _this.navCtrl.popToRoot();
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    RegisterPage.prototype.presentToast = function (text, time) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: time,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    RegisterPage = __decorate([
        Component({
            selector: 'page-register',
            templateUrl: 'register.html',
        }),
        __metadata("design:paramtypes", [NavController, HttpClient, NavParams, AlertController, LoadingController, FormBuilder, ToastController, ModalController, AuthServiceProvider, RuteBaseProvider, StorageProvider])
    ], RegisterPage);
    return RegisterPage;
}());
export { RegisterPage };
//# sourceMappingURL=register.js.map