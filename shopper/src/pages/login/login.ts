import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, ModalController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { ConfirmInfoPage } from '../confirm-info/confirm-info';
import { EmailPasswordPage } from '../email-password/email-password';
import { TabsPage } from '../tabs/tabs';
import { Facebook } from '@ionic-native/facebook';
import { TwitterConnect, TwitterConnectResponse } from '@ionic-native/twitter-connect';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { AuthCredential } from '@firebase/auth-types';
import { ModalInitPage } from '../modal-init/modal-init';
import { StorageProvider } from '../../providers/storage/storage';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  splash = false;
  user: any = {};
  loading: Loading;
  public datos;
  public response;
  public loginUserForm: FormGroup;
  public twitterCredential: AuthCredential;
  public apiuser = {
    nombre: '',
    email: null,
    imagen: 'http://shopper.internow.com.mx/images/user.png',
    telefono: '',
    id_facebook: null,
    id_twitter: null,
    tipo_registro: 0,
    token_notificacion: null
  };

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private loadingCtrl: LoadingController, 
    private facebook: Facebook, 
    private twitter: TwitterConnect, 
    public afAuth: AngularFireAuth, 
    private builder: FormBuilder, 
    public alertCtrl: AlertController, 
    private auth: AuthServiceProvider,
    private modalCtrl: ModalController,
    public storage: StorageProvider
  ) {
    this.initForm();
  }

  ionViewDidLoad() {
    //setTimeout(() => this.splash = false, 4000);
    var item1 = this.storage.get('InitShopper');
    console.log(item1)
    if (item1 == null) {
      this.presentModal();
    }
  }

  initForm() {
    this.loginUserForm = this.builder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      token_notificacion: ['']
    });
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }

  //LOGIN
  login(){
    if (this.loginUserForm.valid) {
      this.showLoading('Iniciando sesión...');
      this.auth.login(this.loginUserForm.value).subscribe(allowed => {
        if (allowed) {        
          this.navCtrl.setRoot(TabsPage);
        } else {
          this.loading.dismiss();
          this.showPopup("Error","Accesso Denegado");
        }
      },
      error => {
        this.loading.dismiss();
        this.showPopup("Error", error.error);
      });
    } else {
      this.showPopup("Error", "Por favor, verifica los datos");
    }
  }

  // LOGIN FACEBOOK
  loginFacebook(){
    this.facebook.login(['public_profile', 'email'])
    .then(rta => {
      if(rta.status == 'connected'){
        this.getInfoFacebook();
      };
    })
    .catch(error =>{
      this.showPopup('Error','Ha ocurrido un error al iniciar sesión con Facebook')
    });
  }

  getInfoFacebook(){
    this.facebook.api('/me?fields=id,name,email,picture.type(large)',['public_profile','email'])
    .then(data=>{
      this.showLoading('Cargando...');
      this.user = data;
      if (this.user.name != null || this.user.name != '') {
        this.apiuser.nombre = this.user.name;
      }
      if (this.user.email != null || this.user.email != '') {
        this.apiuser.email = this.user.email;
      }
      if (this.user.picture != null || this.user.picture != '') {
        this.apiuser.imagen = this.user.picture.data.url;
      }
      if (this.user.id != null || this.user.id != '') {
        this.apiuser.id_facebook = this.user.id;
      }
      this.apiuser.tipo_registro = 2;
      this.auth.loginSocial(this.apiuser).subscribe(allowed => {
        if (allowed) {        
          this.navCtrl.setRoot(TabsPage);
        } else {
          this.loading.dismiss();
          this.showPopup("Error","Accesso Denegado");
        }
      },
      error => {
        this.navCtrl.push(ConfirmInfoPage, {user: this.apiuser});
      });
    })
    .catch(error =>{
      this.loading.dismiss();
      this.showPopup('Error','Ha ocurrido un error al iniciar sesión con Facebook');
    });
  }

  //LOGIN TWITTER
  loginTwitter(){
    this.showLoading('Cargando...');
    this.twitter.login()
    .then((response: TwitterConnectResponse) => {
      var twitterCredential = firebase.auth.TwitterAuthProvider.credential(response.token, response.secret);
      firebase.auth().signInWithCredential(twitterCredential)
      .then(res => {
        this.user = res;
        if (this.user.displayName != null || this.user.displayName != '') {
          this.apiuser.nombre = this.user.displayName;
        }
        if (this.user.providerData[0].email != null || this.user.providerData[0].email != '') {
          this.apiuser.email = this.user.providerData[0].email;
        }
        if (this.user.photoURL != null || this.user.photoURL != '') {
          var picture = this.user.photoURL.replace("_normal","");
          this.apiuser.imagen = picture;
        }
        if (this.user.uid != null || this.user.uid != '') {
          this.apiuser.id_twitter = this.user.uid;
        }
        if (this.user.phoneNumber != null || this.user.phoneNumber != '') {
          this.apiuser.telefono = this.user.phoneNumber;
        }
        this.apiuser.tipo_registro = 3;
        this.auth.loginSocial(this.apiuser).subscribe(allowed => {
          if (allowed) {        
            this.navCtrl.setRoot(TabsPage);
          } else {
            this.loading.dismiss();
            this.showPopup("Error","Accesso Denegado");
          }
        },
        error => {
          this.navCtrl.push(ConfirmInfoPage, {user: this.apiuser});
        });
      })
      .catch((error) => {
        this.loading.dismiss();
        console.log(error);
        this.showPopup('Error',error);
      })
    })
    .catch((error) => {
      console.log(error);
      this.loading.dismiss();
      this.showPopup('Error','Ha ocurrido un error al iniciar sesión con Twitter')
    });
  }

  showLoading(text) {
    this.loading = this.loadingCtrl.create({
      content: text,
      spinner: 'ios',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            
          }
        }
      ]
    });
    alert.present();
  }

  changePassword(){
    this.navCtrl.push(EmailPasswordPage);
  }

  presentModal() {
    let opts = {
      cssClass: 'modal-detail1'
    }
    let profileModal = this.modalCtrl.create(ModalInitPage, {}, opts);
    profileModal.onDidDismiss(data => {
    });
    profileModal.present();
  }

}
