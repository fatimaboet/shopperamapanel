var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { StorageProvider } from '../../providers/storage/storage';
var ModalAccountPage = /** @class */ (function () {
    function ModalAccountPage(navCtrl, navParams, http, builder, viewCtrl, elementService, rutebaseAPI, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.builder = builder;
        this.viewCtrl = viewCtrl;
        this.elementService = elementService;
        this.rutebaseAPI = rutebaseAPI;
        this.storage = storage;
        this.cliente_id = '';
        this.formErrors = {
            'holder_name': '',
            'alias': '',
            'clabe': ''
        };
        this.create_account = {
            token_id: '',
            nombre_titular: '',
            cuenta: '',
            cliente_id: '',
            predeterminado: 2
        };
        this.update_client = {
            customer_id: ''
        };
        this.user = this.storage.getObject('userShopper');
        this.cliente_id = this.user.cliente.id;
        this.update_client.customer_id = navParams.get('token_id');
        this.create_account.predeterminado = navParams.get('predeterminate');
        this.datos3 = navParams.get('data');
        this.initForm();
    }
    ModalAccountPage.prototype.backButtonAction = function () {
        this.viewCtrl.dismiss();
    };
    ModalAccountPage.prototype.initForm = function () {
        var _this = this;
        this.registerAccountForm = this.builder.group({
            holder_name: ['', [Validators.required, Validators.minLength(2)]],
            clabe: ['', [Validators.required, Validators.maxLength(45)]]
        });
        this.registerAccountForm.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
        this.registerAccountForm.controls['clabe'].valueChanges.subscribe(function (selectedValue) {
            if (selectedValue) {
                if (selectedValue.toString().length > 18) {
                    _this.registerAccountForm.patchValue({ clabe: selectedValue.toString().substr(0, 18) });
                }
                ;
            }
            ;
        });
    };
    ModalAccountPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalAccountPage.prototype.addAccount = function (registerAccount) {
        if (this.registerAccountForm.valid) {
            this.elementService.showLoading('Enviando información...');
            this.saveCard();
        }
        else {
            this.validateAllFormFields(this.registerAccountForm);
            this.elementService.presentToast('¡Faltan datos para el registro de la cuenta!');
        }
        ;
    };
    ;
    ModalAccountPage.prototype.saveCard = function () {
        var _this = this;
        this.create_account.cliente_id = this.cliente_id;
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", "Basic " + btoa(this.datos3 + ":"));
        headers = headers.append("Content-Type", "application/json");
        this.http.post(this.rutebaseAPI.getRuteOpen() + 'customers/' + this.update_client.customer_id + '/bankaccounts', this.registerAccountForm.value, {
            headers: headers
        })
            .toPromise()
            .then(function (data) {
            console.log(data);
            _this.datos1 = data;
            _this.create_account.token_id = _this.datos1.id;
            _this.create_account.nombre_titular = _this.registerAccountForm.get('holder_name').value;
            _this.create_account.cuenta = _this.datos1.clabe;
            _this.http.post(_this.rutebaseAPI.getRutaApi() + 'cuentas', _this.create_account)
                .toPromise()
                .then(function (data) {
                _this.elementService.loading.dismissAll();
                _this.elementService.presentToast('Cuenta Bancaria agregada con éxito');
                _this.dismiss();
            }, function (msg) {
                console.log(msg);
                _this.elementService.loading.dismissAll();
            });
        }, function (msg) {
            console.log(msg);
            _this.elementService.loading.dismissAll();
            _this.elementService.presentToast(msg.error.description);
        });
    };
    ModalAccountPage.prototype.onValueChanged = function (data) {
        if (!this.registerAccountForm) {
            return;
        }
        var form = this.registerAccountForm;
        for (var field in this.formErrors) {
            var control = form.get(field);
            this.formErrors[field] = '';
            if (control && control.dirty && !control.valid) {
                for (var key in control.errors) {
                    this.formErrors[field] += true;
                    console.log(key);
                }
            }
        }
    };
    ModalAccountPage.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsDirty({ onlySelf: true });
                _this.onValueChanged();
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    };
    ModalAccountPage = __decorate([
        Component({
            selector: 'page-modal-account',
            templateUrl: 'modal-account.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, HttpClient, FormBuilder, ViewController, ElementsServicesProvider, RuteBaseProvider, StorageProvider])
    ], ModalAccountPage);
    return ModalAccountPage;
}());
export { ModalAccountPage };
//# sourceMappingURL=modal-account.js.map