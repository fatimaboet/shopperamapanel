import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

@Component({
  selector: 'page-modal-init',
  templateUrl: 'modal-init.html',
})
export class ModalInitPage {

  constructor(public navCtrl: NavController, 
  	public navParams: NavParams,
  	public viewCtrl: ViewController,
  	public storage: StorageProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalInitPage');
  }

  dismiss() {
  	   this.storage.set('InitShopper','1');
	   this.viewCtrl.dismiss();
	}

}
