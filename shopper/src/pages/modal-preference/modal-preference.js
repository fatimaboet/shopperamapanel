var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
var ModalPreferencePage = /** @class */ (function () {
    function ModalPreferencePage(navCtrl, navParams, viewCtrl, platform, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.storage = storage;
        this.collection = [];
        this.preference = [];
        this.active = [];
        this.active = navParams.get('prefer');
        this.collection = navParams.get('collec');
        this.initList(this.active);
    }
    ModalPreferencePage.prototype.backButtonAction = function () {
        this.viewCtrl.dismiss(this.preference);
    };
    ModalPreferencePage.prototype.initList = function (active) {
        for (var i = 0; i < this.collection.length; ++i) {
            for (var j = 0; j < active.length; ++j) {
                if (this.collection[i].id == active[j].id) {
                    this.preference.push(active[j]);
                }
            }
        }
    };
    ModalPreferencePage.prototype.onChange = function (event, item) {
        if (event) {
            this.preference.push(item);
        }
        else {
            var index = this.preference.findIndex(function (item1) { return item1.id === item.id; });
            if (index !== -1) {
                this.preference.splice(index, 1);
            }
        }
        ;
    };
    ModalPreferencePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.preference);
    };
    ModalPreferencePage = __decorate([
        Component({
            selector: 'page-modal-preference',
            templateUrl: 'modal-preference.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ViewController, Platform, StorageProvider])
    ], ModalPreferencePage);
    return ModalPreferencePage;
}());
export { ModalPreferencePage };
//# sourceMappingURL=modal-preference.js.map