import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { PollPage } from '../poll/poll';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-accept-question',
  templateUrl: 'accept-question.html',
})
export class AcceptQuestionPage {

	public logo_sucursal: string = '';
	public nombre_sucursal: string = '';
	public cuestionario_id: any;
	public usuario = {
	    nombre: '',
	    imagen: 'assets/imgs/user.png'
	};

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: StorageProvider) {
  	this.logo_sucursal = this.navParams.get('logo');
  	this.nombre_sucursal = this.navParams.get('nombre');
  	this.cuestionario_id = this.navParams.get('cuestionario_id');
  	this.usuario.nombre = this.storage.getObject('userShopper').nombre;
    this.usuario.imagen = this.storage.getObject('userShopper').cliente.imagen;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AcceptQuestionPage');
  }

  acceptQuestion(){
  	this.navCtrl.push(PollPage, {cuestionario_id: this.cuestionario_id, logo: this.logo_sucursal, nombre: this.nombre_sucursal});
  }

  declineQuestion(){
  	this.navCtrl.setRoot(TabsPage);
  }
}
