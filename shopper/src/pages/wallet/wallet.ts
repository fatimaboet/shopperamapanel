import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, AlertController, App, ToastController } from 'ionic-angular';
import { ModalAccountPage } from '../modal-account/modal-account';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { StorageProvider } from '../../providers/storage/storage';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {

	public client_id: string = '';
	public user: any;
	public datos: any;
	public datos1: any;
	public datos2: any;
	public datos3: any;
	public datos4: any;
	public datos5: any;
	public cuentas: any = [];
	public balance: number = 0;
	public predeterminate: number = 1;
	public account_pre: any = {};
	public status: string = '';
	public show_ac: boolean = false;
	public account_predeterminate = {
		predeterminado: 1
	}
	public balance_Account = {
		method: "bank_account",
	    destination_id: "",
	    amount: 0,
	    description: "Retiro de saldo Shopperama",
	    token: ""
	} 

	constructor(public navCtrl: NavController, 
		public navParams: NavParams, 
		public actionSheetCtrl: ActionSheetController, 
		public modalCtrl: ModalController, 
		private http: HttpClient, 
		public rutebaseAPI: RuteBaseProvider, 
		public storage: StorageProvider, 
		private alertCtrl: AlertController, 
		public elementService: ElementsServicesProvider,
		public app: App,
    	private toastCtrl: ToastController) {
		this.user = this.storage.getObject('userShopper');
		this.client_id = this.user.cliente.id;
		this.getAccounts();
	}

	ionViewWillEnter(){
	    this.getInfo();
	}

	getAccounts(){
		this.http.get(this.rutebaseAPI.getRutaApi() + 'cuentas/cliente/'+this.client_id+'?token='+this.storage.get('tokenShopper'))
		.toPromise()
		.then(
		data => {
			console.log(data);
			this.datos = data;
			this.cuentas = this.datos.cuentas;
			for (var i = 0; i < this.cuentas.length; ++i) {
				if (this.cuentas[i].predeterminado == 1) {
					this.account_pre = this.cuentas[i];
				};
			};
		},
		msg => {
			if(msg.status == 400 || msg.status == 401){ 
	          this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
	          this.app.getRootNavs()[0].setRoot(LoginPage);
	        } else {
	          this.presentToast(msg.error.error);
	        }
		});
	}

	getInfo(){
		this.show_ac = false;
		if (this.user.cliente.customer_id != null) {  
	      this.http.get(this.rutebaseAPI.getRutaApi()+'pagos/openpay/customer/'+this.user.cliente.customer_id+'?token='+this.storage.get('tokenShopper'))
	      .toPromise()
	      .then(
	      data => {
	        this.datos1 = data;
	        if (this.datos1.openpay.balance) {
	          this.balance = this.datos1.openpay.balance.toFixed(2);
	        }
	        this.show_ac = true;
	        this.getStatus();
	      },
	      msg => {
	        if(msg.status == 400 || msg.status == 401){ 
	          this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
	          this.app.getRootNavs()[0].setRoot(LoginPage);
	        } else {
	          this.presentToast(msg.error.error);
	        }
	      });
	    } else {
	    	this.show_ac = true;
	      	this.presentAlert();
	    };
	};

	getStatus(){
		if (this.storage.get('idPaySpp')) {  
	      this.http.get(this.rutebaseAPI.getRuteOpen()+'pagos/openpay/customers/'+this.user.cliente.customer_id+'/payouts/'+this.storage.get('idPaySpp'))
	      .toPromise()
	      .then(
	      data => {
	        console.log(data);
	        this.datos4 = data;
	        this.status = this.datos4.openpay.status;
	        if (this.status == 'completed') {
	        	this.storage.remove('idPaySpp');
	        }
	      },
	      msg => {
	      });
	    };
	};

	presentAlert() {
	    let alert = this.alertCtrl.create({
	      title: 'Afiliar cuenta bancaria',
	      message: '¡Para disfrutar del saldo que acumules por tus evaluaciones debes asociar una cuenta bancaria y retirar los fondos!',
	      buttons: [
	        {
	          text: 'Ok',
	          handler: () => {

	          }
	        }
	      ]
	    });
	    alert.present();
	}

	presentActionSheet(number) {
		let actionSheet = this.actionSheetCtrl.create({
		 title: 'Clabe: ' + number.cuenta,
		 buttons: [

		   {
		     text: 'Seleccionar como predeterminada',
		     handler: () => {
		       this.presentConfirm1(number);
		     }
		   },
		   {
		     text: 'Eliminar cuenta',
		     handler: () => {
		       this.presentConfirm(number);
		     }
		   },
		   {
		     text: 'Cancelar',
		     role: 'cancel',
		     handler: () => {
		       console.log('Cancel clicked');
		     }
		   }
		 ]
		});
		actionSheet.present();
	}

	presentProfileModal() {
		if (this.cuentas.length > 0) {
			this.predeterminate = 2;
		};
	    let profileModal = this.modalCtrl.create(ModalAccountPage, { token_id: this.user.cliente.customer_id, predeterminate: this.predeterminate });
	    profileModal.onDidDismiss(data => {
	       this.getAccounts();
	    });
	    profileModal.present();
	}

	presentConfirm(account) {
	  let alert = this.alertCtrl.create({
	    title: 'Eliminar cuenta',
	    message: '¿Desea eliminar la cuenta: '+account.cuenta+'?',
	    buttons: [
	      {
	        text: 'Cancelar',
	        role: 'cancel',
	        handler: () => {
	          console.log('Cancel clicked');
	        }
	      },
	      {
	        text: 'Eliminar',
	        handler: () => {
	          this.deleteAccount(account);
	        }
	      }
	    ]
	  });
	  alert.present();
	}

	presentConfirm1(account) {
	  let alert = this.alertCtrl.create({
	    title: 'Seleccionar como predeterminada',
	    message: '¿Desea seleccionar la cuenta: '+account.cuenta+' como predeterminada?',
	    buttons: [
	      {
	        text: 'Cancelar',
	        role: 'cancel',
	        handler: () => {
	          console.log('Cancel clicked');
	        }
	      },
	      {
	        text: 'Aceptar',
	        handler: () => {
	          this.selectAccount(account);
	        }
	      }
	    ]
	  });
	  alert.present();
	}

	selectAccount(account){
		this.elementService.showLoading('Enviando información...');
		this.http.put(this.rutebaseAPI.getRutaApi()+'cuentas/'+account.id+'?token='+this.storage.get('tokenShopper'), this.account_predeterminate)
		.toPromise()
		.then(
		data => {
			this.getAccounts();
			this.elementService.loading.dismissAll();
			this.elementService.presentToast('Cuenta Bancaria seleccionada como predeterminada');
		},
		msg => {
		console.log(msg);
		this.elementService.loading.dismissAll();
		});
	}

	deleteAccount(account){
		this.elementService.showLoading('Eliminando cuenta bancaria...');
	    this.http.delete(this.rutebaseAPI.getRutaApi()+'pagos/openpay/customers/'+this.user.cliente.customer_id+'/bankaccounts/'+account.token_id)
	    .toPromise()
	    .then(
	    data => {
	    	this.datos5 = data;
	    	if (this.datos5.openpay == null) {
	    		this.http.delete(this.rutebaseAPI.getRutaApi()+'cuentas/'+account.id+'?token='+this.storage.get('tokenShopper'))
				.toPromise()
				.then(
				data => {
					this.getAccounts();
					this.elementService.loading.dismissAll();
					this.elementService.presentToast('Cuenta bancaria eliminada con éxito');
				},
				msg => {
					this.elementService.loading.dismissAll();
					if(msg.status == 400 || msg.status == 401){ 
					  this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
					  this.app.getRootNavs()[0].setRoot(LoginPage);
					} else {
					  this.presentToast(msg.error.error);
					}
				});   
	    	}  
	    },
	    msg => {
	      	this.elementService.loading.dismissAll();
	      	if(msg.status == 400 || msg.status == 401){ 
	          this.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
	          this.app.getRootNavs()[0].setRoot(LoginPage);
	        } else {
	          this.presentToast(msg.error.error);
	        }
	    });
	}

	checkAccount(){
		if (this.account_pre.cuenta != undefined) {
			this.presentConfirmRet();
		} else {
			this.elementService.presentToast('Debe agregar una cuenta bancaria para retirar el saldo');
		}
	}

	presentConfirmRet() {
	  let alert = this.alertCtrl.create({
	    title: 'Retirar Saldo',
	    subTitle: '¿Desea retirar los fondos a la cuenta: '+this.account_pre.cuenta+'?',
	    message: 'AVISO: al retirar el saldo acreditado se descontará del mismo la cantidad de $8.00 MXN por transacción SPI',
	    cssClass: 'msg-alert',
	    buttons: [
	      {
	        text: 'Cancelar',
	        role: 'cancel',
	        handler: () => {
	          console.log('Cancel clicked');
	        }
	      },
	      {
	        text: 'Aceptar',
	        handler: () => {
	          this.removingBalance();
	        }
	      }
	    ]
	  });
	  alert.present();
	}

	removingBalance(){
		this.elementService.showLoading('Solicitando transacción...');
		this.balance_Account.amount = this.balance;
		this.balance_Account.destination_id = this.account_pre.token_id;
		this.balance_Account.token = this.storage.get('tokenShopper');
		this.http.post(this.rutebaseAPI.getRutaApi()+'pagos/openpay/customers/'+this.user.cliente.customer_id+'/payouts', this.balance_Account)
		.toPromise()
		.then(
		data => {
			console.log(data);
			this.datos3 = data;
			this.elementService.loading.dismissAll();
			if (this.datos3.openpay.status == "in_progress") {
				this.storage.set('idPaySpp',this.datos3.openpay.id);				
	        	this.presentAlert1();
	        	this.getInfo();
			}
		},
		msg => {
			console.log(msg);
			this.elementService.loading.dismissAll();
	        this.elementService.presentToast('Ha ocurrido un error al retirar el saldo');
		});
	};

	presentAlert1() {
	    let alert = this.alertCtrl.create({
	      title: 'Retiro de Fondos',
	      message: '¡Su solicitud ha sido procesada con éxito! EL pago se realizará en un lapso de 1 día hábil bancario.',
	      buttons: [
	        {
	          text: 'Ok',
	          handler: () => {

	          }
	        }
	      ]
	    });
	    alert.present();
	}

	presentToast(text) {
	    let toast = this.toastCtrl.create({
	      message: text,
	      duration: 3000,
	      position: 'top'
	    });

	    toast.onDidDismiss(() => {
	      console.log('Dismissed toast');
	    });

	    toast.present();
	}
}

