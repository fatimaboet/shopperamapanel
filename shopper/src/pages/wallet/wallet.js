var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, AlertController } from 'ionic-angular';
import { ModalAccountPage } from '../modal-account/modal-account';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { StorageProvider } from '../../providers/storage/storage';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
var WalletPage = /** @class */ (function () {
    function WalletPage(navCtrl, navParams, actionSheetCtrl, modalCtrl, http, rutebaseAPI, storage, alertCtrl, elementService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.rutebaseAPI = rutebaseAPI;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.elementService = elementService;
        this.client_id = '';
        this.cuentas = [];
        this.balance = 0;
        this.predeterminate = 1;
        this.account_pre = {};
        this.status = '';
        this.account_predeterminate = {
            predeterminado: 1
        };
        this.balance_Account = {
            method: "bank_account",
            destination_id: "",
            amount: 0,
            description: "Retiro de saldo Shopperama"
        };
        this.user = this.storage.getObject('userShopper');
        this.client_id = this.user.cliente.id;
        this.getAccounts();
    }
    WalletPage.prototype.ionViewWillEnter = function () {
        this.getInfo();
    };
    WalletPage.prototype.getAccounts = function () {
        var _this = this;
        this.http.get(this.rutebaseAPI.getRutaApi() + 'cuentas/cliente/' + this.client_id)
            .toPromise()
            .then(function (data) {
            _this.datos = data;
            _this.cuentas = _this.datos.cuentas;
            console.log(_this.cuentas);
            for (var i = 0; i < _this.cuentas.length; ++i) {
                if (_this.cuentas[i].predeterminado == 1) {
                    _this.account_pre = _this.cuentas[i];
                }
                ;
            }
            ;
            console.log(_this.account_pre);
        }, function (msg) {
        });
    };
    WalletPage.prototype.getInfo = function () {
        var _this = this;
        this.http.get(this.rutebaseAPI.getRutaApi() + 'sistema/tarifas/pk')
            .toPromise()
            .then(function (data) {
            _this.datos2 = data;
            if (_this.user.cliente.customer_id != null) {
                var headers = new HttpHeaders();
                headers = headers.append("Authorization", "Basic " + btoa(_this.datos2.tarifas.tarifa_sk + ":"));
                headers = headers.append("Content-Type", "application/json");
                _this.http.get(_this.rutebaseAPI.getRuteOpen() + 'customers/' + _this.user.cliente.customer_id, {
                    headers: headers
                })
                    .toPromise()
                    .then(function (data) {
                    _this.datos1 = data;
                    _this.balance = _this.datos1.balance.toFixed(2);
                    _this.getStatus();
                }, function (msg) {
                });
            }
            else {
                _this.presentAlert();
            }
            ;
        }, function (msg) {
        });
    };
    ;
    WalletPage.prototype.getStatus = function () {
        var _this = this;
        if (this.storage.get('idPaySpp') != null || this.storage.get('idPaySpp') != '') {
            var headers = new HttpHeaders();
            headers = headers.append("Authorization", "Basic " + btoa(this.datos2.tarifas.tarifa_sk + ":"));
            headers = headers.append("Content-Type", "application/json");
            this.http.get(this.rutebaseAPI.getRuteOpen() + 'customers/' + this.user.cliente.customer_id + '/payouts/trl9jsekdmfpsi4yjt1o', {
                headers: headers
            })
                .toPromise()
                .then(function (data) {
                console.log(data);
                _this.datos4 = data;
                _this.status = _this.datos4.status;
                if (_this.status == 'completed') {
                    _this.storage.remove('idPaySpp');
                }
            }, function (msg) {
            });
        }
        ;
    };
    ;
    WalletPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Afiliar cuenta bancaria',
            message: '¡Para disfrutar del saldo que acumules por tus evaluaciones debes asociar una cuenta bancaria y retirar los fondos!',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    WalletPage.prototype.presentActionSheet = function (number) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Clabe: ' + number.cuenta,
            buttons: [
                {
                    text: 'Seleccionar como predeterminada',
                    handler: function () {
                        _this.presentConfirm1(number);
                    }
                },
                {
                    text: 'Eliminar cuenta',
                    handler: function () {
                        _this.presentConfirm(number);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    WalletPage.prototype.presentProfileModal = function () {
        var _this = this;
        if (this.cuentas.length > 0) {
            this.predeterminate = 2;
        }
        ;
        var profileModal = this.modalCtrl.create(ModalAccountPage, { token_id: this.user.cliente.customer_id, data: this.datos2.tarifas.tarifa_sk, predeterminate: this.predeterminate });
        profileModal.onDidDismiss(function (data) {
            _this.getAccounts();
        });
        profileModal.present();
    };
    WalletPage.prototype.presentConfirm = function (account) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Eliminar cuenta',
            message: '¿Desea eliminar la cuenta: ' + account.cuenta + '?',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Eliminar',
                    handler: function () {
                        _this.deleteAccount(account);
                    }
                }
            ]
        });
        alert.present();
    };
    WalletPage.prototype.presentConfirm1 = function (account) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Seleccionar como predeterminada',
            message: '¿Desea seleccionar la cuenta: ' + account.cuenta + ' como predeterminada?',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Aceptar',
                    handler: function () {
                        _this.selectAccount(account);
                    }
                }
            ]
        });
        alert.present();
    };
    WalletPage.prototype.selectAccount = function (account) {
        var _this = this;
        this.elementService.showLoading('Enviando información...');
        this.http.put(this.rutebaseAPI.getRutaApi() + 'cuentas/' + account.id, this.account_predeterminate)
            .toPromise()
            .then(function (data) {
            _this.getAccounts();
            _this.elementService.loading.dismissAll();
            _this.elementService.presentToast('Cuenta Bancaria seleccionada como predeterminada');
        }, function (msg) {
            console.log(msg);
            _this.elementService.loading.dismissAll();
        });
    };
    WalletPage.prototype.deleteAccount = function (account) {
        var _this = this;
        this.elementService.showLoading('Eliminando cuenta bancaria...');
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", "Basic " + btoa(this.datos2.tarifas.tarifa_sk + ":"));
        headers = headers.append("Content-Type", "application/json");
        this.http.delete(this.rutebaseAPI.getRuteOpen() + 'customers/' + this.user.cliente.customer_id + '/bankaccounts/' + account.token_id, {
            headers: headers
        })
            .toPromise()
            .then(function (data) {
            _this.http.delete(_this.rutebaseAPI.getRutaApi() + 'cuentas/' + account.id)
                .toPromise()
                .then(function (data) {
                _this.getAccounts();
                _this.elementService.loading.dismissAll();
                _this.elementService.presentToast('Cuenta bancaria eliminada con éxito');
            }, function (msg) {
                _this.elementService.loading.dismissAll();
                _this.elementService.presentToast('Ha ocurrido un error al eliminar la cuenta');
            });
        }, function (msg) {
            _this.elementService.loading.dismissAll();
            _this.elementService.presentToast('Ha ocurrido un error al eliminar la cuenta');
        });
    };
    WalletPage.prototype.presentConfirmRet = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Retirar Saldo',
            subTitle: '¿Desea retirar los fondos a la cuenta: ' + this.account_pre.cuenta + '?',
            message: 'AVISO: al retirar el saldo acreditado se descontará del mismo la cantidad de $8.00 MXN por transacción SPI',
            cssClass: 'msg-alert',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Aceptar',
                    handler: function () {
                        _this.removingBalance();
                    }
                }
            ]
        });
        alert.present();
    };
    WalletPage.prototype.removingBalance = function () {
        var _this = this;
        this.elementService.showLoading('Solicitando transacción...');
        this.balance_Account.amount = this.balance;
        this.balance_Account.destination_id = this.account_pre.token_id;
        var headers = new HttpHeaders();
        headers = headers.append("Authorization", "Basic " + btoa(this.datos2.tarifas.tarifa_sk + ":"));
        headers = headers.append("Content-Type", "application/json");
        this.http.post(this.rutebaseAPI.getRuteOpen() + 'customers/' + this.user.cliente.customer_id + '/payouts', this.balance_Account, {
            headers: headers
        })
            .toPromise()
            .then(function (data) {
            console.log(data);
            _this.datos3 = data;
            if (_this.datos3.status == "in_progress") {
                _this.storage.set('idPaySpp', _this.datos3.id);
                _this.elementService.loading.dismissAll();
                _this.presentAlert1();
                _this.getInfo();
            }
            ;
        }, function (msg) {
            console.log(msg);
            _this.elementService.loading.dismissAll();
            _this.elementService.presentToast('Ha ocurrido un error al eliminar la cuenta');
        });
    };
    ;
    WalletPage.prototype.presentAlert1 = function () {
        var alert = this.alertCtrl.create({
            title: 'Retiro de Fondos',
            message: '¡Su solicitud ha sido procesada con éxito! EL pago se realizará en un lapso de 1 día hábil bancario.',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    WalletPage = __decorate([
        Component({
            selector: 'page-wallet',
            templateUrl: 'wallet.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ActionSheetController, ModalController, HttpClient, RuteBaseProvider, StorageProvider, AlertController, ElementsServicesProvider])
    ], WalletPage);
    return WalletPage;
}());
export { WalletPage };
//# sourceMappingURL=wallet.js.map