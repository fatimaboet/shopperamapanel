import { Component } from '@angular/core';

import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { EvaluationsPendingPage } from '../evaluations-pending/evaluations-pending';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = EvaluationsPendingPage;
  tab3Root = ContactPage;

  constructor() {

  }
}
