import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, ToastController, ModalController, Keyboard } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { TabsPage } from '../tabs/tabs';
import { ModalPreferencePage } from '../modal-preference/modal-preference';

@Component({
  selector: 'page-confirm-info',
  templateUrl: 'confirm-info.html',
})
export class ConfirmInfoPage {

	user: any = {};
	loading: Loading;
  public registerUserForm: FormGroup;
	public estados: any;
  public municipios: any = [];
	public ciudades: any = [];
  public colonias: any = [];
	public datos: any;
  public datos1: any;
  public datos2: any;
  public datos3: any;
  public dataPreference: any = [];
  public createSuccess: boolean = false;
  public preferences: any = [];
  sexos = [{id:0, tipo: 'Femenino'}, {id:1, tipo: 'Masculino'}];
  city: string = 'Municipio';
	formErrors = {
		'nombre': '',
		'telefono': '',
		'email': '',
		'ciudad': '',
		'estado': ''
	};
  public input: string = '';
  public countries: string[] = [];

	constructor(
    public navCtrl: NavController, 
    private auth: AuthServiceProvider, 
    public navParams: NavParams, 
    private http: HttpClient, 
    private loadingCtrl: LoadingController, 
    private builder: FormBuilder, 
    public alertCtrl: AlertController, 
    private toastCtrl: ToastController, 
    private rutebaseApi: RuteBaseProvider, 
    public modalCtrl: ModalController,
    private keyboard: Keyboard
  ) {
		this.user = navParams.get('user');
		this.initForm();
	}

	initForm() {
    this.registerUserForm = this.builder.group({
      nombre: [this.user.nombre, [Validators.required, Validators.minLength(2)]],
      email: [this.user.email, [Validators.required, Validators.email]],
      edad: ['', [Validators.required]],
      tipo_registro: [this.user.tipo_registro],
      sexo: ['Femenino'],
      estado_id: [''], 
      municipio_id: [''], 
      localidad_id: [''],
      imagen: [this.user.imagen],
      preferencias: [''],
      id_facebook: [this.user.id_facebook],
      id_twitter: [this.user.id_twitter],
      id_instagram: [this.user.id_instagram],
      token_notificacion: [this.user.token_notificacion],
      input: ['']
    });
    this.registerUserForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
    this.http.get(this.rutebaseApi.getRutaApi() + 'categorias')
    .toPromise()
    .then(
      data => {
        this.dataPreference = data;
        this.page();
      },
      msg => {
        this.page();
        this.presentToast('No se pudo cargar las preferencias, ingresa de nuevo',1500);
    });
   }

  page(){
    this.showLoadingc();
    this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/estados')
    .toPromise()
    .then(
      data => {
        this.datos = data;
        this.estados = this.datos.estados;
        this.estados = this.sortByKey(this.estados, 'nombre');
        this.registerUserForm.patchValue({estado_id: this.estados[0].id}); 
        this.setEstado(this.estados[0].id);
        this.loading.dismiss();
      },
      msg => {
        this.presentToast('No se pudo cargar los estados y ciudades, ingresa de nuevo',1500);
        this.loading.dismiss();
    });
  }

  setEstado(estado){
    this.municipios = [];
    this.colonias = [];
    this.initEstados(estado);
    if (estado == 9) {
      this.city = 'Delegación';
    } else {
      this.city = 'Municipio';
    }
  }

  initEstados(estado_id){
    this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/municipios?estado_id='+estado_id)
    .toPromise()
    .then(
      data => {
        this.datos1 = data;
        this.municipios = this.datos1.municipios;
        this.municipios = this.sortByKey(this.municipios,'nombre');
        this.registerUserForm.patchValue({municipio_id: this.municipios[0].id});
        this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id='+this.municipios[0].id)
        .toPromise()
        .then(
          data => {
            this.datos2 = data;
            this.colonias = this.datos2.localidades;
            this.colonias = this.sortByKey(this.colonias,'nombre');
            this.registerUserForm.patchValue({localidad_id: this.colonias[0].id});
          },
          msg => {
            this.presentToast('No se pudo cargar las colonias, intenta de nuevo',1500);
        });
      },
      msg => {
        this.presentToast('No se pudo cargar los municipios, intenta de nuevo',1500);
    });
  }

  setMunicipio(event){
    this.colonias = [];
    this.http.get(this.rutebaseApi.getRutaApi() + 'mx/get/localidades?municipio_id='+event)
    .toPromise()
    .then(
      data => {
        this.datos2 = data;
        this.colonias = this.datos2.localidades;
        this.colonias = this.sortByKey(this.colonias,'nombre');
        this.registerUserForm.patchValue({localidad_id: this.colonias[0].id});
        this.registerUserForm.patchValue({input: this.colonias[0].nombre});
      },
      msg => {
        this.presentToast('No se pudo cargar las colonias, intenta de nuevo',1500);
    });
  }

  setColonia(event){
    this.registerUserForm.patchValue({localidad_id: event});
  }

  search() {
    if (!this.registerUserForm.value.input.trim().length || !this.keyboard.isOpen()) {
      this.countries = [];
      return;
    }
    
    this.countries = this.colonias.filter(item => item.nombre.toUpperCase().includes(this.registerUserForm.value.input.toUpperCase()));
  }

  add(item) {
    this.registerUserForm.patchValue({input: item.nombre});
    this.registerUserForm.patchValue({localidad_id: item.id});
    this.countries = [];
  }

  removeFocus() {
    this.keyboard.close();
  }

  presentModal() {
    if(this.dataPreference != ''){
      let profileModal = this.modalCtrl.create(ModalPreferencePage, { prefer: this.preferences, collec: this.dataPreference.categorias });
      profileModal.onDidDismiss(data => {
       this.preferences = data;
      });
      profileModal.present();
    } else {
      this.presentToast('No se pudieron cargar las preferencias ingresa de nuevo',1500);
    } 
  }

  deleteChip(prefer){
    let index = this.preferences.findIndex((item1) => item1.nombre === prefer.nombre);
    if(index !== -1){
      this.preferences.splice(index, 1);
    }
    for (var i = 0; i < this.dataPreference.categorias.length; ++i) {
      if (this.dataPreference.categorias[i].nombre == prefer.nombre) {
        this.dataPreference.categorias[i].checked = false;
      } 
    }
  }

  register(){
    this.registerUserForm.value.preferencias = JSON.stringify(this.preferences);
    this.registerUserForm.value.email = this.registerUserForm.value.email.toLowerCase();
    if (this.registerUserForm.valid) {
      this.showLoading();
      this.auth.registerSocial(this.registerUserForm.value).subscribe(
        success => {
          if (success) {
            this.loading.dismiss();
            this.createSuccess = true;
            this.showPopup("Completado", 'Usuario registrado con éxito');
          } else {
            this.presentToast("Ha ocurrido un error al crear la cuenta.", 1500);
          }
        },
        error => {
          this.loading.dismiss();
          this.showPopup("Error", error.error);
        }
      );
    } else {
      this.validateAllFormFields(this.registerUserForm);
      this.presentToast('¡Faltan datos para el registro!', 1500);
    }
  }

  onValueChanged(data?: any) {
    if (!this.registerUserForm) { return; }
    const form = this.registerUserForm;

    for (const field in this.formErrors) { 
      const control = form.get(field);
      this.formErrors[field] = '';
      if (control && control.dirty && !control.valid) {
        for (const key in control.errors) {
          this.formErrors[field] += true;
          console.log(key);
        }
      } 
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsDirty({ onlySelf:true });
        this.onValueChanged();
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  public sortByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Registrando Usuario...',
      spinner: 'ios',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showLoadingc() {
    this.loading = this.loadingCtrl.create({
      content: 'Cargando...',
      spinner: 'ios'
    });
    this.loading.present();
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.navCtrl.setRoot(TabsPage);
            }
          }
        }
      ]
    });
    alert.present();
  }

  presentToast(text,time) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: time,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
