import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { NavController, NavParams, App, LoadingController, Loading, ModalController, AlertController, Content, ActionSheetController, Platform } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RuteBaseProvider } from '../../providers/rute-base/rute-base';
import { ElementsServicesProvider } from '../../providers/elements-services/elements-services';
import { LoginPage } from '../login/login';
import { Camera } from '@ionic-native/camera';
import { NgProgress } from '@ngx-progressbar/core';
import { OcrProvider } from '../../providers/ocr/ocr';
import { Events } from 'ionic-angular';
import { WizardAnimations } from '../../components/wizard/ion-simple-wizard-animations';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { CropImageComponent } from '../../components/crop-image/crop-image';
import { ImageViewerController } from 'ionic-img-viewer';
import { StorageProvider } from '../../providers/storage/storage';
import { FilePath } from '@ionic-native/file-path';

declare var cordova: any;

@Component({
  selector: 'page-poll',
  templateUrl: 'poll.html',
  animations: WizardAnimations.btnRotate
})
export class PollPage {
	public poll: any = [];
	public poll_name: any = '';
	public poll_question: any = [];
	public logo_sucursal: string = '';
	public nombre_sucursal: string = '';
	public cuestionario_id: any;
  public datos: any;
  public selectedImage: string = 'assets/imgs/ticket-view.jpg';
  public selectedImage1 = 'assets/imgs/price.png';
  public imageText: string = '';
  public _zone: any;
  public lastImage: string = null;
  public loading: Loading;
  public respuesta: string = '';
  public mount_payment: string = '';
  public sucursales: any = [];
  public count_cuest = 0;

  public step: any;
  public stepCondition: any;
  public stepDefaultCondition: any;
  public currentStep: any;

  public create_respuesta = {
    cuestionario: '',
    imagen_factura: '',
    estado_pagado: 1,
    monto_pagado: '',
    campana_id: '',
    sucursal_id: '',
    cliente_id: '',
    cuestionario_id: '',
    empresa_id: ''
  };
  _imageViewerCtrl: ImageViewerController;
  @ViewChild('imageViewer') input: ElementRef;
  @ViewChild('imageViewer1') input1: ElementRef;
  @ViewChild(Content) content: Content;
  image1 = '';
  image2 = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public element:ElementRef, 
    public http: HttpClient, 
    public rutebaseAPI: RuteBaseProvider, 
    public elementService: ElementsServicesProvider, 
    public app: App, 
    private camera: Camera, 
    public progress: NgProgress, 
    public loadingCtrl: LoadingController, 
    public ocr: OcrProvider, 
    public evts: Events,  
    private file: File, 
    private transfer: Transfer,
    private modalCtrl: ModalController,
    private imageViewerCtrl: ImageViewerController,
    public storage: StorageProvider,
    private alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    private platform: Platform, 
    private filePath: FilePath) {
  	this.logo_sucursal = this.navParams.get('logo');
  	this.nombre_sucursal = this.navParams.get('nombre');
  	this.cuestionario_id = this.navParams.get('cuestionario_id');
    this.create_respuesta.cuestionario_id = this.cuestionario_id;
    this.create_respuesta.campana_id = this.navParams.get('campana_id');
    this.create_respuesta.cliente_id = this.navParams.get('client_id');
    this.initQuestion();
    this._zone = new NgZone({ enableLongStackTrace: false });
    this.step = 1;
    this.stepCondition = false;
    this.stepDefaultCondition = this.stepCondition;
    this._imageViewerCtrl = imageViewerCtrl;

    this.evts.subscribe('step:changed', step => {
      this.currentStep = step;
      this.stepCondition = this.stepDefaultCondition;
    });

    this.image1 = this.file.applicationDirectory+'www/assets/imgs/ticket.png';
    this.image2 = this.file.applicationDirectory+'www/assets/imgs/ticket-price.png';

    this.evts.subscribe('step:next', () => {
      if (this.currentStep == 2) {
        setTimeout(()=>{
          this.content.scrollToTop();
        },500);  
        if (this.create_respuesta.imagen_factura == '') {
          this.stepCondition = false;
        } else {
          this.stepCondition = true;
        }
      }
      /*if (this.currentStep == 3) {
        if (this.imageText == '') {
          this.stepCondition = false;
        } else {
          this.stepCondition = true;
        }
      }*/
      if (this.currentStep == 3) {
        setTimeout(()=>{
          this.content.scrollToTop();
        },500);  
        if (this.mount_payment == '') {
          this.stepCondition = false;
        } else {
          this.stepCondition = true;
        }
      }
      if (this.currentStep == 4) {
        this.stepCondition = true;
      }    
    });
    this.evts.subscribe('step:back', () => {
      this.stepCondition = true;
    });
  }

  ionViewWillLeave() {
    this.evts.unsubscribe('step:changed');
    this.evts.unsubscribe('step:next');
    this.evts.unsubscribe('step:back');
  }

  initQuestion(){
    this.elementService.showLoading('Cargando Evaluación...');
    this.http.get(this.rutebaseAPI.getRutaApi()+'cuestionarios/'+this.cuestionario_id+'?token='+this.storage.get('tokenShopper'))
    .toPromise()
    .then(
      data => {
        this.datos = data;
        this.poll = JSON.parse(this.datos.cuestionario.cuestionario);
        this.poll_name = this.poll.nombre;
        this.poll_question = this.poll.preguntas;
        this.create_respuesta.empresa_id = this.datos.cuestionario.campana.empresa_id;
        this.sucursales = this.datos.cuestionario.campana.sucursales;
        this.create_respuesta.sucursal_id = this.sucursales[0].id;
        for (var i = 0; i < this.poll_question.length; ++i) {
          this.poll_question[i].rpta = ''; 
        }
        this.elementService.hideLoading();
      },
      msg => {
        this.elementService.hideLoading();
        if(msg.status == 400 || msg.status == 401){ 
          this.elementService.presentToast(msg.error.error + ', Por favor inicia sesión de nuevo');
          this.app.getRootNav().setRoot(LoginPage);
        } else {
          this.elementService.presentToast(msg.error.error);
        }
    });
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccione una Imagen',
      buttons: [
        {
          text: 'Cargar de Librería',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Usar Camara',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 80,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
   
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.elementService.presentToast('Error al seleccionar la imagen');
    });
  }
 
   // Create a new name for the image
  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }
   
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.uploadImage();
    }, error => {
      this.elementService.presentToast('Error al guardar la imagen');
    });
  }
   
  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImage() {
    // Destination URL
    var url = "http://shopper.internow.com.mx/images_uploads/upload.php";
   
    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
   
    // File name only
    var filename = this.lastImage;
   
    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };
   
    const fileTransfer: TransferObject = this.transfer.create();
   
    this.loading = this.loadingCtrl.create({
      content: 'Subiendo Imagen...',
      spinner: 'ios'
    });
    this.loading.present();
    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      this.loading.dismissAll()
      this.selectedImage = data.response;
      this.create_respuesta.imagen_factura = data.response;
      this.stepCondition = true;
    }, err => {
      this.loading.dismissAll();
      this.elementService.presentToast('Error al subir la imagen');
      this.selectedImage = 'assets/imgs/ticket-view.jpg';
    });
  }

 
  /*getPicture1() {
    this.camera.getPicture({
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then((imageData) => {
      let base64Image: string = "data:image/jpeg;base64," + imageData;
      this.selectedImage1 = base64Image;
      this.cropImage(base64Image,500,500);
    }, (err) => {
      console.log(err);
      this.elementService.presentToast('Error al seleccionar la imagen');
      //this.stepCondition = true;//
    });
  }

  toBase64(url: string) {
    return new Promise(function (resolve) {
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                resolve(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.send();
    });
  };*/

  /*cropImage(image: string, width: number, height: number) {
    let cropModal = this.modalCtrl.create(CropImageComponent, { "imageBase64": image, "width": width, "height": height });
    cropModal.onDidDismiss((croppedImage: any) => {
      if (!croppedImage)
        console.log("Canceled while cropping.");
      else {
        console.log(croppedImage);*/
        /*this.loading = this.loadingCtrl.create({
          content: 'Verificando monto...',
          spinner: 'ios'
        });
        this.loading.present();
        var that = this;*/
        /*setTimeout(function() {
          this.ocr.recognizeText(croppedImage).then(function(result) {
            that._zone.run(() => {          
              if (result != '') {
                that.imageText = result;
                that.create_respuesta.monto_pagado = result;
                that.stepCondition = true;
                that.loading.dismissAll();
              } else {
                that.loading.dismissAll();
                that.stepCondition = false;
                that.elementService.presentToast('No hemos podido obtener el monto del ticket, por favor sube una imagen más visible');
              }
            });
          });
        })*/
        /*this.recognize(croppedImage);
      }
    });
    cropModal.present();
  }

  public async recognize(img): Promise<void> {
    const loading = await this.loadingCtrl.create({
      content: 'Analyzing image',
    });

    await loading.present();

    try {
      const value = await this.ocr.recognizeText(img, (progress) => {
        const percentage = Math.round(progress * 100)
        loading.setContent(`Analysing image (${percentage}%)`);
      });
      this.imageText = value;
    } finally {
      await loading.dismiss();
    }
  }*/

  viewReferenceTicket(){
    const imageViewer = this._imageViewerCtrl.create(this.input.nativeElement);
    imageViewer.present();
  }

  viewREference(){
    const imageViewer = this._imageViewerCtrl.create(this.input1.nativeElement);
    imageViewer.present();
  }

  onFinish(){
    var count = 0;
    for (var i = 0; i < this.poll_question.length; ++i) {
      if (this.poll_question[i].rpta == '') {
        count = 1;
      }
    }
    if (count == 1) {
      this.elementService.presentToast('Debes completar todas las preguntas del cuestionario');
      return true;
    }
    if (this.create_respuesta.imagen_factura == '') {
      this.elementService.presentToast('Debes subir una imagen del ticket');
      return true;
    }
    if (this.create_respuesta.monto_pagado == '') {
      this.elementService.presentToast('Debes ingresar un monto del ticket');
      return true;
    }
    this.create_respuesta.cuestionario = JSON.stringify(this.poll_question);
    this.loading = this.loadingCtrl.create({
      content: 'Verificando información del ticket y el comercio, por favor espere...',
      spinner: 'ios'
    });
    this.loading.present();
    this.http.post(this.rutebaseAPI.getRutaApi()+'evaluaciones?token='+this.storage.get('tokenShopper'), this.create_respuesta)
    .toPromise()
    .then(
    data => {
      this.loading.dismissAll();
      this.showPopup('Evaluación enviada con éxito', 'Su evaluación se envió a revisión, en breve será atendida.')
    },
    msg => {
      this.loading.dismissAll();
      console.log(msg.error);
      this.elementService.presentToast(msg.error);
    });
  }

  changeQuestion(item){
    var count = 0;
    for (var i = 0; i < this.poll_question.length; ++i) {
      if (this.poll_question[i].rpta == '') {
        count =+ 1;
      }
    }
    if (count > 0) {
      this.stepCondition = false;
    } else {
      this.stepCondition = true;
    }
  }

  changeMount(mount_payment){
    if (mount_payment == '') {
      this.stepCondition = false;
    } else {
      this.create_respuesta.monto_pagado = mount_payment;
      this.stepCondition = true;
    }
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            this.navCtrl.popToRoot();
          }
        }
      ]
    });
    alert.present();
  }
}
