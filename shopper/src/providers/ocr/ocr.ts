import { Injectable } from '@angular/core';
import Tesseract from 'tesseract.js/dist/tesseract.js';
import { File } from '@ionic-native/file';

export type ProgressFn = (progress: number) => void;

@Injectable()
export class OcrProvider {

	private tesseract;
	private rootDir;

	constructor(private file: File) {
		//this.file.listDir(this.file.applicationDirectory, 'www/assets/lib/').then(res => alert(JSON.stringify(res))).catch(err => alert(JSON.stringify(err)));
		this.file.checkDir(this.file.applicationDirectory, 'www/assets/lib/').then(
			(exists) => {
            	if (exists) {
            		this.rootDir = this.file.applicationDirectory+'www/assets/lib/';
            		this.tesseract = Tesseract.create({
				      langPath: this.rootDir+'tesseract.js-',
				      corePath: this.rootDir+'tesseract.js-core.js',
				      workerPath: this.rootDir+'tesseract.js-worker.js',
				    });
            	} else {

            	}
          	},
          	(err) => {
          	}	
		).catch(err => console.log('Directory doesn\'t exist'));
	}

	/*public recognizeText(image){
		const tesseractConfig = {
		  lang: 'spa', 
		  tessedit_char_whitelist: '0123456789,.',
		  preserve_interword_spaces: 0
		};

		return new Promise<string>((resolve, reject) => {
		  this.tesseract.recognize(image, tesseractConfig)
		    //.progress((v) => console.log(v))
		    .catch((err) => reject(err))
		    .then((result) => resolve(result.text));
		});
	}*/

	public recognizeText(image: any, progressCallback: ProgressFn): Promise<string> {
    // Wrap the Tesseract process inside a native Promise,
    // as the PromiseLike returned by Tesseract caused problems.
    return new Promise<string>((resolve, reject) => {
      const tesseractConfig = {
        lang: 'spa', 
		tessedit_char_whitelist: '0123456789,.',
		preserve_interword_spaces: 0
      }

      this.tesseract.recognize(image, tesseractConfig)
        .progress((status) => {
          if (progressCallback != null) {
            const progress = status.status == 'recognizing text'
              ? status.progress
              : 0

            progressCallback(progress)
          }
        })
        .catch((err) => {
          console.error('OcrProvider.text: Failed to analyze text.', err)

          reject(err)
        })
        .then((result) => resolve(result.text));
    });
  }

}