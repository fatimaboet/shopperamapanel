var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import Tesseract from 'tesseract.js/dist/tesseract.js';
import { File } from '@ionic-native/file';
var OcrProvider = /** @class */ (function () {
    function OcrProvider(file) {
        var _this = this;
        this.file = file;
        //this.file.listDir(this.file.applicationDirectory, 'www/assets/lib/').then(res => alert(JSON.stringify(res))).catch(err => alert(JSON.stringify(err)));
        this.file.checkDir(this.file.applicationDirectory, 'www/assets/lib/').then(function (exists) {
            if (exists) {
                _this.rootDir = _this.file.applicationDirectory + 'www/assets/lib/';
                _this.tesseract = Tesseract.create({
                    langPath: _this.rootDir + 'tesseract.js-',
                    corePath: _this.rootDir + 'tesseract.js-core.js',
                    workerPath: _this.rootDir + 'tesseract.js-worker.js',
                });
            }
            else {
            }
        }, function (err) {
        }).catch(function (err) { return console.log('Directory doesn\'t exist'); });
    }
    OcrProvider.prototype.recognizeText = function (image) {
        var _this = this;
        var tesseractConfig = {
            lang: 'spa',
            tessedit_char_whitelist: '0123456789,.',
            preserve_interword_spaces: 0
        };
        return new Promise(function (resolve, reject) {
            _this.tesseract.recognize(image, tesseractConfig)
                .progress(function (v) { return console.log(v); })
                .catch(function (err) { return reject(err); })
                .then(function (result) { return resolve(result.text); });
        });
    };
    OcrProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [File])
    ], OcrProvider);
    return OcrProvider;
}());
export { OcrProvider };
//# sourceMappingURL=ocr.js.map