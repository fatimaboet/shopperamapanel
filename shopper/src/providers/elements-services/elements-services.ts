import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, Loading, ToastController } from 'ionic-angular';

@Injectable()
export class ElementsServicesProvider {

	loading: Loading;

	constructor(public http: HttpClient, private loadingCtrl: LoadingController, private toastCtrl: ToastController) {
		console.log('Hello ElementsServicesProvider Provider');
	}

	showLoading(text) {
		this.loading = this.loadingCtrl.create({
		  content: text,
		  spinner: 'ios',
		  duration: 25000
		});
		this.loading.present();
	}

	presentToast(text) {
		let toast = this.toastCtrl.create({
		  message: text,
		  duration: 3000,
		  position: 'top'
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
	}

	hideLoading(){
		this.loading.dismiss();
	}

}
