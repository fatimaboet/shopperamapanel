import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RuteBaseProvider {

	//Remoto
	public api_base = 'https://shopper.internow.com.mx/shoppersAPI/public/';
	//public ruta_open = "https://api.openpay.mx/v1/myglvxzieyy48cmhfrnv/";
	public ruta_open = "https://sandbox-api.openpay.mx/v1/mlfpboaflbbaboev97wm/";

	constructor(public http: HttpClient) {
	}

	getRutaApi(){
		return this.api_base;
	}

	getRuteOpen(){
		return this.ruta_open;
	}

}
